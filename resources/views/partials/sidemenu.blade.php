<aside class="column is-2 is-narrow-mobile is-fullheight section is-hidden-mobile is-hidden-tablet-only">
    <p class="menu-label is-hidden-touch">Navigation (Pusat)</p>
    <ul class="menu-list">
        <li>
            <a class="target-link" href="dashboard#welcome">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Dashboard
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#product">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Barang / Jasa
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#cart">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Penjualan
            </a>
        </li>
        
        <li>
            <a class="target-link" href="dashboard#account">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Akun
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#transaction">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Laporan Transaksi
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#tdokter">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Transaksi Dokter
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#tjasa">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Transaksi Jasa
            </a>
        </li>

        {{-- <li>
            <a class="target-link" href="dashboard#points">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Poin Saya
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#post">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Tulisan
            </a>
        </li> --}}
        {{-- @if($account->level_id == 1)
        <li>
            <a class="target-link" href="dashboard#file">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data File
            </a>
        </li>
        <li>
            <a class="target-link" href="dashboard#reward">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Reward
            </a>
        </li>
        @endif --}}
        {{-- <li>
            <a href="https://staging.violettabeauty.co.id/expert/test">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Sistem Expert
            </a>
        </li> --}}
        <!-- <li>
            <a href="#" class="is-active">
                <span class="icon"><i class="fa fa-table"></i></span> Links
            </a>
    
            <ul>
                <li>
                    <a href="#">
                        <span class="icon is-small"><i class="fa fa-link"></i></span> Link1
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon is-small"><i class="fa fa-link"></i></span> Link2
                    </a>
                </li>
            </ul>
        </li> -->
    </ul>
    @if($account->level_id == 1)
    <p class="menu-label is-hidden-touch">Other</p>
    <ul class="menu-list">
        <li>
            <a class="target-link" href="dashboard#setting">
                <span class="icon">
                    <i class="typcn typcn-cog"></i>
                </span>
                Setting
            </a>
        </li>
    </ul>
    @endif
</aside>