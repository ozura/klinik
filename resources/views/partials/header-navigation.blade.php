<nav class="navbar is-success">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{url('/')}}">
            <img src="{{asset('images/header-logo.png')}}" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
        </a>

        <div class="navbar-burger burger" data-target="main-navigation">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div id="main-navigation" class="navbar-menu">
        <div class="navbar-start is-hidden-desktop">
            <a class="navbar-item target-link" href="dashboard#welcome">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Dashboard
            </a>
            <a class="navbar-item target-link" href="dashboard#product">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Barang Milik
            </a>
            <a class="navbar-item target-link" href="dashboard#cart">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Penjualan
            </a>
            <a class="navbar-item target-link" href="dashboard#transaction">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Laporan Penjualan
            </a>
            <a class="navbar-item target-link" href="dashboard#account">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Agen
            </a>
            <a class="navbar-item target-link" href="dashboard#points">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Poin Saya
            </a>
            <a class="navbar-item target-link" href="dashboard#post">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Tulisan
            </a>
            <a class="navbar-item" href="https://staging.violettabeauty.co.id/expert/test">>
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Sistem Expert
            </a>
            @if($account->level_id == 1)
            <a class="navbar-item target-link" href="dashboard#file">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data File
            </a>
            <a class="navbar-item target-link" href="dashboard#reward">
                <span class="icon">
                    <i class="typcn typcn-chevron-right"></i>
                </span>
                Data Reward
            </a>
            <a class="navbar-item target-link" href="dashboard#setting">
                <span class="icon">
                    <i class="typcn typcn-cog"></i>
                </span>
                Setting
            </a>
            @endif
            <!-- <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link" href="#">
                    <span class="icon"><i class="fa fa-table"></i></span> Links
                </a>
                <div class="navbar-dropdown">
                    <a class="navbar-item " href="#">
                        Link 1
                    </a>
                    <a class="navbar-item " href="#">
                        Link 2
                    </a>
                </div>
            </div> -->
        </div>
        <div class="navbar-end">
            <a class="navbar-item modal-button" data-target="#modal-change-password">
                <span class="icon"><i class="typcn typcn-key"></i></span> Ubah Password
            </a>
            <a class="navbar-item" href="{{url('signout')}}"">
                <span class="icon"><i class="typcn typcn-power"></i></span> Keluar
            </a>
        </div>
    </div>
</nav>