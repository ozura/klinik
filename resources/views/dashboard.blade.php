@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('app')
@section('content')
@include('partials/header-navigation', compact('account'))
<section class="columns is-fullheight">
    @include('partials/sidemenu', compact('account'))
    <div class="section column is-10" id="appcontent">
    </div>
</section>
<div id="modal-change-password" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head is-primary-color">
            <p class="modal-card-title">Change Password</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <form id="form-change-password" method="POST" action="{{url('ajax/password/change')}}">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Old Password</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="password" name="old_password" minlength="4" required="">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-key"></i>
                            </span>
                            <p class="help"></p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">New Password</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input id="new_password" class="input" type="password" name="new_password" minlength="4" required="">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-key"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Confirm new Password</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="password" name="new_confirm_password" minlength="4" required="" equalto="#new_password">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-key"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <div class="field is-grouped is-grouped-right">
                <div class="control">
                    <!-- <button class="button is-link is-primary-color" onclick="actionChangePassword()">Save</button> -->
                    <button type="submit" class="button is-link is-primary-color">Save</button>
                </div>
            </form>
                <div class="control">
                    <button class="button is-cancel is-text">Cancel</button>
                </div>
            </div>
        </footer>
    </div>
</div>
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $(document).ready(function  () {
        var original_title = location.hash;
        var target_url = original_title.replace('#','');
        if (target_url == '' || target_url == '/' || target_url == 'undefined') {
            loadURI('welcome');
        }else{
            loadURI(target_url);
        }
    });

    window.onhashchange = function() {
        // if (window.innerDocClick) {
            //Your own in-page mechanism triggered the hash change
        // } else {
            //Browser back button was clicked
            var original_title = location.hash;
            var target_url = original_title.replace('#','');
            if (target_url == '' || target_url == '/' || target_url == 'undefined') {
                loadURI('welcome');
            }else{
                loadURI(target_url);
            }
        // }
    }

    function loadURI(target_url, content) {
        content = typeof content !== 'undefined' ? content : 'appcontent';
        NProgress.start();
        $.ajax({
            type: "GET",
            url: base_url + target_url,
            contentType: false,
            success: function (data) {
                $("#" + content).html(data);
                location.hash = target_url;

                NProgress.done();
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    }

    $('#form-change-password').validate({
        highlight: function (input) {
            $(input).addClass('is-danger');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.control').addClass('help').addClass('is-danger').append(error);
        },
        submitHandler: function(form) {
            $('button').attr('disabled', 'disabled');
            $('input').attr('readonly', 'readonly');

            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(result) {
                    if(result.status == 200){
                        iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                        closeModalHelper();
                        $(form)[0].reset();
                    }else{
                        iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                    }
                },
                complete: function() {
                    $('button').removeAttr('disabled', 'disabled');
                    $('input').removeAttr('readonly', 'readonly');
                }
            });
        }
    });

    function summernoteUploadsImage(image, notekey) {
        var data = new FormData();
        data.append("image_base", image);
        $.ajax({
            url: "{{action('PostController@actionUploadImage')}}",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function(url) {
                var image = $('<img>').attr('src', url);
                $(notekey).summernote("insertNode", image[0]);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endpush