@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('app')
@section('content')
<section class="hero is-fullheight is-primary-color">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="columns">
                <div class="column is-4 is-offset-4">
                    <div class="box">
                        <figure>
                            <img src="{{asset('images/logo.png')}}" width="174" height="128">
                        </figure>
                        <p class="subtitle has-text-grey">Please sign in to proceed.</p>
                        <div class="is-gap">
                            <form id="form-signin" method="POST" action="{{url('signin')}}">
                                {{ csrf_field() }}
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="email" placeholder="email" name="username" value="{{Request::old('username')}}" required="">
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="password" placeholder="password" name="password" required="" minlength="4">
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control width-100">
                                        <button class="button is-success width-100">Sign In</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="is-gap">
                            <p class="has-text-grey">
                                Violetta agent management system
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $('#form-signin').validate({
        highlight: function (input) {
            $(input).addClass('is-danger');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.control').addClass('help').addClass('is-danger').append(error);
        }
    });
</script>
@endpush