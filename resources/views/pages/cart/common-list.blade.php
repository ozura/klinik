<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content" style="margin-bottom: 2rem;">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>

                <div class="level-right">
                </div>
            </nav>
        </div>
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <div class="level-item">
                        <a class="button is-black is-primary-color modal-button" data-target="#modal-add-item">
                            <span class="icon is-small">
                                <i class="typcn typcn-plus"></i>
                            </span>
                            <span>Tambah Barang</span>
                        </a>
                    </div>
                    <div class="level-item">
                        <a class="button is-warning modal-button" data-target="#modal-customer">
                            <span class="icon is-small">
                                <i class="typcn typcn-user"></i>
                            </span>
                            <span>Customer</span>
                        </a>
                    </div>
                    <div class="level-item">
                        @if(!empty($cart->customer_id))
                        <a class="button is-text" id="customer-label" onclick="actionDeleteCustomerCart(this)">Customer: {{$cart->customer->name}}</a>
                        <input type="hidden" name="customer" value="{{$cart->customer_id}}" />
                        @else
                        <a class="button is-text" id="customer-label" onclick="actionDeleteCustomerCart(this)">Customer: guest</a>
                        <input type="hidden" name="customer" value="0" />
                        @endif
                    </div>
                </div>

                <div class="level-right">
                    <p class="level-item">
                        <a class="button is-black is-primary-color modal-button" data-target="#modal-customer" >
                            <span class="icon is-small">
                                <i class="typcn typcn-arrow-forward"></i>
                            </span>
                            <span>Checkout</span>
                        </a>
                    </p>
                </div>
            </nav>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div id="modal-add-item" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="width: 80%">
        <header class="modal-card-head is-primary-color">
            <p class="modal-card-title">Tambah Barang</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <div class="content" style="overflow-x:scroll;">
                <table class="table is-striped is-bordered is-fullwidth" style="width: 100%" id="secondary_table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Gambar</th>
                            <th>Sku</th>
                            <th>Nama</th>
                            <th>Tersedia</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<div id="modal-customer" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="width: 80%">
        <header class="modal-card-head is-primary-color">
            <p class="modal-card-title">Pilih Customer</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <div class="content" style="overflow-x:scroll;">
                <table class="table is-striped is-bordered is-fullwidth" style="width: 100%" id="tertiary_table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/cart/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'image.image_url', searchable: false, orderable: false,
                render: function(data){
                    return '<img style="width: 75px;" src='+ base_url + data +'>';
                }
            },
            { data: 'name' },
            { data: 'quantity', searchable: false, orderable: false },
            { data: 'selling_price', searchable: false, orderable: false },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-table is-black is-primary-color" onclick="addQuantityItemAction(this)" data-id="'+data.id+'" data-number="1">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-plus"></i>'+
                    '    </span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-table is-black is-primary-color" onclick="addQuantityItemAction(this)" data-id="'+data.id+'" data-number="-1">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-minus"></i>'+
                    '    </span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteItemAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/product/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'image.image_url', searchable: false, orderable: false,
                render: function(data){
                    return '<img style="width: 75px;" src='+ base_url + data +'>';
                }
            },
            { data: 'sku' },
            { data: 'name' },
            { data: 'final_stock', searchable: false, orderable: false },
            { data: 'selling_price', searchable: false, orderable: false,
                render: function(data) {
                    let html = '';
                    $.each(data, function( key, value ) {
                        html += value + '<br>';
                    });
                    return html;
                }
            },
            { data: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-table is-black is-primary-color" onclick="addItemAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-plus"></i>'+
                    '    </span>'+
                    '</a>';
                }
            }
        ]
    });

    secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    var tertiary_table = $('#tertiary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/customer/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'name' },
            { data: 'email' },
            { data: 'status', searchable: false, orderable: false, },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-black is-primary-color" onclick="checkoutAction(this)" data-id="'+data.id+'" data-name="'+data.name+'">'+
                    '   Pilih'+
                    '</a>';
                }
            }
        ]
    });

    tertiary_table.on( 'draw', function () {
        tertiary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function addItemAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            async: false,
            url: base_url + 'ajax/cart/add',
            data: {
                product_id: item.attr('data-id')
            },
            success: function (result) {
                item.removeClass('is-loading');
                if(result.status_code == 200){
                    iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                    primary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                }
            }
        });
    };

    function addQuantityItemAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            async: false,
            url: base_url + 'ajax/cart/add-quantity',
            data: {
                cart_detail_id: item.attr('data-id'),
                quantity: item.attr('data-number')
            },
            success: function (result) {
                item.removeClass('is-loading');
                if(result.status_code == 200){
                    iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                    primary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                }
            }
        });
    };

    function deleteItemAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Apa Anda yakin?',
            text: "Aksi ini tidak bisa dibatalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url + 'ajax/cart/delete',
                    async: false,
                    data: {
                        cart_detail_id: item.attr('data-id')
                    },
                    success: function (result) {
                        item.removeClass('is-loading');
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            primary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }else{
                item.removeClass('is-loading');
            }
        });
    };

    function actionAddCustomerCart(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
        $.ajax({
            type: "POST",
            async: false,
            url: base_url + 'ajax/cart/customer/add',
            data: {
                customer_id: item.attr('data-id')
            },
            success: function (result) {
                item.removeClass('is-loading');
                if(result.status_code == 200){
                    $('html').removeClass('is-clipped');
                    $('.modal').removeClass('is-active');
                    $('#customer-label').html('Customer: ' + item.attr('data-name'));
                    $('input[name=customer]').val(item.attr('data-id'));
                    iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                    primary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                }
            }
        });
    };

    function actionDeleteCustomerCart(element){
        if($('input[name=customer]').val() != 0){
            var item = $(element);
            if(item.hasClass('is-loading')){
                return false;
            }else{
                item.addClass('is-loading');
            }

            swal({
                title: 'Apa Anda yakin?',
                text: "Aksi ini tidak bisa dibatalkan",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + 'ajax/cart/customer/delete',
                        async: false,
                        success: function (result) {
                            item.removeClass('is-loading');
                            if(result.status_code == 200){
                                $('#customer-label').html('Customer: guest');
                                $('input[name=customer]').val(0);
                                iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                                primary_table.ajax.reload(null, false);
                            }else{
                                iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                            }
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
            });
        }
    };

    function checkoutAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        var CHECKOUT_KEY = 'PtwwBO0VwkWXa0C9RKtX';
        swal({
            title: 'Apa Anda yakin?',
            text: "Aksi ini tidak bisa dibatalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: base_url + 'ajax/cart/checkout',
                    data: {
                        key: CHECKOUT_KEY,
                        dokter: item.attr('data-id'),
                        customer: $('input[name=customer]').val()
                    },
                    success: function (result) {
                        item.removeClass('is-loading');
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            var transaction_number = result.data.transaction_number;
                            swal({
                                title: 'Print nota?',
                                text: "Transaksi sukses, print notanya Kak!",
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Yes'
                                }).then((result) => {
                                    if (result.value) {
                                        printJS({printable: base_url + 'transaction/print?transaction_number=' + transaction_number, type: 'pdf', showModal: true});
                                    }
                                    loadURI('cart');
                            });
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }else{
                item.removeClass('is-loading');
            }
        });
    }
</script>