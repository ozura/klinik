<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>

                <div class="level-right">
                    <div class="level-item">
                        {{-- @if($account->level_id == 1) --}}
                        <a class="button is-black is-primary-color target-link" href="dashboard#product/add">
                            <span class="icon is-small">
                                <i class="typcn typcn-plus"></i>
                            </span>
                            <span>Tambah Barang Baru</span>
                        </a>
                        {{-- @endif --}}
                    </div>
                </div>
            </nav>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <table class="table is-hoverable is-fullwidth" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        {{-- <th>Gambar</th> --}}
               {{--          <th>Sku</th> --}}
                        <th>Nama</th>
                        {{-- <th>Tersedia</th> --}}
                        <th>Type</th>
                        <th>Harga</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/product/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            // { data: 'product.image.image_url', searchable: false, orderable: false,
            //     render: function(data){
            //         return '<img style="width: 75px;" src='+ base_url + data +'>';
            //     }
            // },
            
            
            { data: 'name' },
            { data: 'type' },
            // { data: 'final_stock', searchable: false, orderable: false },
            { data: 'harga' },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    let html = '';
                    @if($account->level_id == 1)
                    html += '<a class="button is-rounded is-table is-small is-success target-link" href="dashboard#product/edit/' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '</a>';
                    @endif
                    html += '<a class="button is-rounded is-table is-small is-warning" onclick="saveStockAction(this)" data-id="' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-cog"></i>'+
                    '    </span>'+
                    '</a>';

                    return html;
                }
            }
        ]
    });
    
    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    async function saveStockAction(element) {
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        const {value: stock} = await Swal({
            title: 'Update stock Anda?',
            input: 'number',
            confirmButtonText: 'Submit'
        })

        if (stock) {
            $.ajax({
                type: "POST",
                url: base_url + 'ajax/product/stock/save',
                async: false,
                data: {
                    id: item.attr('data-id'),
                    stock: stock
                },
                success: function (result) {
                    item.removeClass('is-loading');
                    if(result.status_code == 200){
                        iziToast.success({ title: 'Good job', message: result.message, position: 'topRight' });
                        primary_table.ajax.reload(null, false);
                    }else{
                        iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                    }
                },
            });
        }else{
            item.removeClass('is-loading');
        }
    }
</script>