<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content" style="margin-bottom: 2rem;">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>
            </nav>
        </div>
        <div class="content">
            <p class="title is-5">Selamat datang {{$account->name}} di sistem keagenan Violetta</p>
        </div>
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class=<p class="level-item">
                        <p class="control">
                            <label><b>Mulai</b></label>
                        </p>
                    </p>
                    <p class="level-item">
                        <p class="control">
                            <input class="is-datepicker" type="datetime" value="{{Carbon\Carbon::now('Asia/Jakarta')->startOfWeek()}}" name="start_date">
                        </p>
                    </p>
                    <p class="level-item">
                        <p class="control">
                            <label><b>Hingga</b></label>
                        </p>
                    </p>
                    <p class="level-item">
                        <p class="control">
                            <input class="is-datepicker" type="datetime" value="{{Carbon\Carbon::now('Asia/Jakarta')->endOfWeek()}}" name="end_date">
                        </p>
                    <p class="level-item">
                        <p class="control">
                            <button class="button is-black is-primary-color" onclick="changeChart()">
                                Filter Tanggal
                            </button>
                        </p>
                    </p>
                </div>

            </nav>
        </div>
        <div class="content" style="width: 100%;">
            <canvas id="transaction-canvas"></canvas>
        </div>
    </div>
</div>
<div class="columns">
    <div class="column">
        <div class="card is-gap">
            <div class="card-content">
                <div class="content" style="width: 100%;">
                    <p class="subtitle is-5"><strong>Produk terjual</strong></p>
                    <canvas id="product-canvas"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="card is-gap">
            <div class="card-content">
                <div class="content" style="margin-bottom: 2rem;">
                    <p class="subtitle is-5"><strong>Berita dan artikel terbaru</strong></p>
                </div>
                <div class="content">
                    @foreach($posts as $post)
                    <a class="target-link" href="dashboard#post/detail/{{$post->post_id}}">
                        <div class="box">
                            <article class="media">
                                <div class="media-left">
                                    <figure class="image is-64x64"> <img src="{{url($post->image->image_url)}}">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <div class="content">
                                        <p class="subtitle is-6">{{$post->title}}</p>
                                        {!!$post->plain_text!!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    </a>
                    @endforeach
                </div>
                <hr>
                <div class="content" style="margin-bottom: 2rem;">
                    <p class="subtitle is-5"><strong>File file training online</strong></p>
                </div>
                <div class="content">
                    @foreach($files as $file)
                    <a target="_blank" href="{{url($file->file_url)}}">
                        <div class="box">
                            <article class="media">
                                <div class="media-content">
                                    <div class="content">
                                        <p class="subtitle is-6">{{$file->name}}.{{$file->type}}</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let primary_chart_data = {
        labels: [''],
        datasets: [{
            label: '',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            borderWidth: 1,
            fill: false,
            data: [0]
        }]
    };

    let secondary_chart_data = {
        labels: [''],
        datasets: [{
            label: '',
            backgroundColor: 'rgb(255, 99, 132)',
            data: [0]
        }]
    };

    initCalendar();
    initCanvas();

    function changeChart(){
        let data1 = [];
        let data2 = [];
        let color = [];

        $.ajax({
            type: "POST",
            url: base_url + 'ajax/transaction/chart',
            data: {
                startdate: encodeURIComponent($('input[name=start_date]').val()),
                enddate: encodeURIComponent($('input[name=end_date]').val()),
            },
            success: function (result) {
                primary_chart_data.labels.splice(-1 * primary_chart_data.labels.length, primary_chart_data.labels.length); // remove the label first
                $.each(result, function( key, value ) {
                    primary_chart_data.labels.push(key);
                    data1.push(value);
                });
                
                let newDataset = {
                    label: 'Penjualan',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    borderWidth: 1,
                    fill: false,
                    data: data1
                };
                
                primary_chart_data.datasets.pop();
                primary_chart_data.datasets.push(newDataset);
                window.primary_canvas.update();
            }
        });

        $.ajax({
            type: "POST",
            async: false,
            url: base_url + 'ajax/transaction/product/chart',
            data: {
                startdate: encodeURIComponent($('input[name=start_date]').val()),
                enddate: encodeURIComponent($('input[name=end_date]').val()),
            },
            success: function (result) {
                secondary_chart_data.labels.splice(-1 * secondary_chart_data.labels.length, secondary_chart_data.labels.length); // remove the label first
                $.each(result, function( key, value ) {
                    secondary_chart_data.labels.push(key);
                    color.push(dynamicColors());
                    data2.push(value);
                });
                
                let newDataset = {
                    label: 'Produk terjual (pcs)',
                    backgroundColor: color,
                    data: data2
                };
                
                secondary_chart_data.datasets.pop();
                secondary_chart_data.datasets.push(newDataset);
                window.secondary_canvas.update();
            }
        });
    };

    function initCanvas(){
        var ctx = document.getElementById('transaction-canvas').getContext('2d');
        window.primary_canvas = new Chart(ctx, {
            type: 'line',
            data: primary_chart_data,
            options: {
                responsive: true,
                elements: {
                    line: {
                        tension: 0.000001
                    }
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        });

        var ctx = document.getElementById('product-canvas').getContext('2d');
        window.secondary_canvas = new Chart(ctx, {
            type: 'pie',
            data: secondary_chart_data,
            options: {
                responsive: true
            }
        });
        changeChart();   
    }

    function dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
</script>