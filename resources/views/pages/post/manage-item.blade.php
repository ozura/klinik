<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<form id="form-manage-item" method="POST" action="{{url('ajax/post/submit')}}">
    <div class="columns">
        <div class="column is-8">
            <div class="card is-gap">
                <div class="card-content">
                    <div class="content">
                        <nav class="level">
                            <div class="level-left">
                                <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                            </div>
                        </nav>
                    </div>
                    <div class="content">
                        <input type="hidden" name="id" @if(!empty($item)) value="{{$item->post_id}}" @endif>
                        <div class="field">
                            <label class="label">Judul*</label>
                            <p class="control">
                                <input class="input" type="text" name="title" required="" @if(!empty($item)) value="{{$item->title}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Kategori*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="post_category_id" required="">
                                        <option value="">Pilih kategori</option>
                                        @foreach($post_categories as $post_category)
                                        @if(!empty($item) && $item->post_category_id == $post_category->post_category_id)
                                        <option value="{{$post_category->post_category_id}}" selected>{{$post_category->name}}</option>
                                        @else
                                        <option value="{{$post_category->post_category_id}}">{{$post_category->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Thumbnail</label>
                        </div>
                        <div class="field">
                            <div class="content">
                                <figure>
                                    @if(!empty($item) && !empty($item->image->image_url))
                                    <img id="cover-image" src="{{url($item->image->image_url)}}">
                                    @else
                                    <img id="cover-image" src="https://bulma.io/images/placeholders/256x256.png"> 
                                    @endif
                                </figure>
                                <input class="input" type="hidden" name="image"> 
                            </div>
                        </div>
                        <div class="field">
                            <div class="file is-primary">
                                <label class="file-label">
                                    <input class="file-input" type="file" id="file-image" accept="image/png, image/jpeg, image/jpg">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                            <i class="fa fa-upload"></i>
                                        </span>
                                        <span class="file-label">
                                            Pilih gambar
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Isi tulisan</label>
                            <div class="control">
                                @if(!empty($item))
                                <textarea id="item-content" name="content">{!!$item->content!!}</textarea>
                                @else
                                <textarea id="item-content" name="content"></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="card is-gap">
                <div class="card-content">
                    <nav class="level">
                        <div class="level-left">
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <div class="box">
                                    <div class="field is-horizontal">
                                        <div class="field body is-grouped is-grouped-right">
                                            <div class="control">
                                                <button class="button is-link is-primary-color">
                                                    <span class="icon">
                                                        <i class="fa fa-save"></i>
                                                    </span>
                                                    <span>Simpan</span>
                                                </button>
                                            </div>
                                            <div class="control">
                                                <a class="target-link" href="dashboard#post">
                                                    <button class="button is-text">Kembali</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript" src="{{asset('vendor/compressorjs/compressor.min.js')}}"></script>
<script>
    initAutoNumeric();
    $('#form-manage-item').validate({
        highlight: function (input) {
            $(input).addClass('is-danger');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.control').addClass('help').addClass('is-danger').append(error);
        },
        submitHandler: function(form) {
            $('button').attr('disabled', 'disabled');
            $('input').attr('readonly', 'readonly');
            
            setTimeout(() => {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(result) {
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            loadURI(result.loaduri);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    },
                    complete: function() {
                        $('button').removeAttr('disabled', 'disabled');
                        $('input').removeAttr('readonly', 'readonly');
                    }
                });
                
            }, 1000);
        }
    });

    $('#item-content').summernote({
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], '#item-content');
            }
        }
    });

    document.getElementById('file-image').addEventListener('change', (e) => {
        const file = e.target.files[0];

        if (!file) {
            return;
        }

        new Compressor(file, {
            quality: 0.6,
            success(result) {
                var reader = new FileReader();
                reader.readAsDataURL(result); 
                reader.onloadend = function() {
                    base64data = reader.result;                
                    $('input[name=image]').val(base64data);
                    $('#cover-image').attr('src', base64data)
                }
            },
            error(err) {
                console.log(err.message);
            },
        });
    });
</script>