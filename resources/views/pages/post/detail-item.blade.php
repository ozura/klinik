<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <p class="title is-5">{{$item->title}}</p>
        </div>
        <div style="padding-bottom:1rem;">
            <figure class="image">
                <img src="{{url($item->image->image_url)}}">
            </figure>
        </div>
        <div class="content">
            {!!$item->content!!}
        </div>
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <p>Ditulis oleh Admin</p>
        <p>Kategori: {{$item->category->name}}</p>
    </div>
</div>