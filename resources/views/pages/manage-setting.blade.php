<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<form id="form-manage-item" method="POST" action="{{url('ajax/setting/submit')}}">
    <div class="columns">
        <div class="column is-8">
            <div class="card is-gap">
                <div class="card-content">
                    <div class="content">
                        <nav class="level">
                            <div class="level-left">
                                <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                            </div>
                        </nav>
                    </div>
                    <div class="content">
                        <div class="field">
                            <label class="label">Poin yang didapat saat setiap x subtotal transaksi</label>
                            <p class="control">
                                <input class="input is-number" type="text" name="points_get" required="" value="{{$settings->where('name', 'points_get')->first()->content}}">
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Subtotal barang yang dibayar untuk mendapat sejumlah x poin (Berlaku kelipatan)</label>
                            <p class="control">
                                <input class="input is-currency" type="text" name="to_get_points" required="" value="{{$settings->where('name', 'to_get_points')->first()->content}}">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="card is-gap">
                <div class="card-content">
                    <nav class="level">
                        <div class="level-left">
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <div class="box">
                                    <div class="field is-horizontal">
                                        <div class="field body is-grouped is-grouped-right">
                                            <div class="control">
                                                <button class="button is-link is-primary-color">
                                                    <span class="icon">
                                                        <i class="fa fa-save"></i>
                                                    </span>
                                                    <span>Simpan</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    initAutoNumeric();
    $('#form-manage-item').validate({
        highlight: function (input) {
            $(input).addClass('is-danger');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.control').addClass('help').addClass('is-danger').append(error);
        },
        submitHandler: function(form) {
            $('button').attr('disabled', 'disabled');
            $('input').attr('readonly', 'readonly');
            
            setTimeout(() => {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(result) {
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    },
                    complete: function() {
                        $('button').removeAttr('disabled', 'disabled');
                        $('input').removeAttr('readonly', 'readonly');
                    }
                });
                
            }, 1000);
        }
    });
</script>