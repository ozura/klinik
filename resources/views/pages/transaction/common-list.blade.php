<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>

                <div class="level-right">
                    <p class="level-item">
                        <p class="control">
                            <label><b>Mulai</b></label>
                        </p>
                    </p>
                    <p class="level-item">
                        <p class="control">
                            <input class="is-datepicker" type="datetime" value="{{Carbon\Carbon::now('Asia/Jakarta')->startOfWeek()}}" name="start_date">
                        </p>
                    </p>
                    <p class="level-item">
                        <p class="control">
                            <label><b>Hingga</b></label>
                        </p>
                    </p>
                    <p class="level-item">
                        <p class="control">
                            <input class="is-datepicker" type="datetime" value="{{Carbon\Carbon::now('Asia/Jakarta')->endOfWeek()}}" name="end_date">
                        </p>
                    <p class="level-item">
                        <p class="control">
                            <button class="button is-black is-primary-color" onclick="filterAction()">
                                Filter Tanggal
                            </button>
                        </p>
                    </p>
                </div>
            </nav>
        </div>
        <div class="content" style="width: 75%;">
            <canvas id="canvas"></canvas>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Transaksi</th>
                        <th>Customer</th>
                        <th>Tanggal Transaksi</th>
                        <th>Total Transaksi</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div id="modal-detail-transaksi" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="width: 100%;">
        <header class="modal-card-head is-primary-color">
            <p class="modal-card-title" id="modal-title">Transaksi</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="content">
                <table class="table is-striped is-bordered is-fullwidth" id="secondary_table" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Name</th>
                            <th>Harga</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>

<script>
    let primary_chart_data = {
        labels: [''],
        datasets: [{
            label: '',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            borderWidth: 1,
            fill: false,
            data: [0]
        }]
    };

    initCalendar();
    initCanvas();

    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, 100, -1 ],
            [ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
        ],
        buttons: [
            'pageLength',
            {
                extend: 'print',
                text: 'PDF',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        ajax: {
            url: base_url + 'ajax/transaction/table',
            type: 'POST',
            data: function(params){
                params.startdate = encodeURIComponent($('input[name=start_date]').val());
                params.enddate = encodeURIComponent($('input[name=end_date]').val());
            },
        },
        columns: [
            { data: 'index_table', defaultContent: '', searchable: false, orderable: false },
            { data: 'transaction_number', name: 'transaction_number' },
            { data: 'customer_name', name: 'customer.name' },
            { data: 'transaction_date', name: 'transaction_date', searchable: false, orderable: false },
            { data: 'subtotal', name: 'subtotal', searchable: false, orderable: false, 
                render: function(data){
                    if(data.status == 'normal'){
                        return data.subtotal;
                    }else{
                        return '<b style="color: #f46842;">Cancel</b>';
                    }
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false, 
                render: function(data){
                    let html = '<a class="button is-small is-rounded is-table is-black is-primary-color modal-button" onclick="showDetailAction(this)" data-target="#modal-detail-transaksi" data-id="'+data.id+'" data-number="'+data.number+'">'+
                        '    <span class="icon is-small">'+
                        '        <i class="typcn typcn-eye"></i>'+
                        '    </span>'+
                        '</a>'+
                        '<a class="button is-small is-rounded is-table is-black" onclick="printAction(this)" data-number="'+data.number+'">'+
                        '    <span class="icon is-small">'+
                        '        <i class="typcn typcn-printer"></i>'+
                        '    </span>'+
                        '</a>';
                    if(data.status == 'normal'){
                        return html +
                        '<a class="button is-small is-rounded is-table is-danger is-outlined" onclick="returItemAction(this)" data-number="'+data.number+'">'+
                        '    <span class="icon is-small">'+
                        '        <i class="typcn typcn-delete-outline"></i>'+
                        '    </span>'+
                        '    <span>Cancel</span>'+
                        '</a>';
                    }else{
                        return html;
                    }
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
            primary_table.cell(cell).invalidate('dom');
        } );
    } ).draw();

    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/transaction/detail/table',
            type: 'POST'
        },
        columns: [
            { data: 'index_table', defaultContent: '', searchable: false, orderable: false },
            { data: 'product.sku', name: 'product.sku' },
            { data: 'product.name', name: 'product.name'},
            { data: 'price', name: 'price', searchable: false, orderable: false, },
            { data: 'quantity', name: 'quantity' },
        ]
    });

    secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();
    
    function showDetailAction(element){
        var item = $(element);
        $('#modal-title').html('Transaksi '+item.attr('data-number'));
        secondary_table.ajax.url(base_url + 'ajax/transaction/detail/table?transaction_id='+item.attr('data-id')).load(null, false);
    };

    function printAction(element){
        var item = $(element);
        var nota = item.attr('data-number');
        printJS({printable: base_url + 'transaction/print?transaction_number=' + nota, type: 'pdf', showModal: true});
    };

    function returItemAction(element){
        var item = $(element);
        swal({
            title: 'Apa Anda yakin?',
            text: "Aksi ini tidak bisa dibatalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: base_url + 'api/transaction/retur',
                    data: {
                        NO_TRANSAKSI: item.attr('data-nomor')
                    },
                    success: function (result) {
                        if(result.status == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            primary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }
        });
    };

    function filterAction(){
        primary_table.draw();
        changeChart();
    };

    function changeChart(){
        let data = [];

        $.ajax({
            type: "POST",
            async: false,
            url: base_url + 'ajax/transaction/chart',
            data: {
                startdate: encodeURIComponent($('input[name=start_date]').val()),
                enddate: encodeURIComponent($('input[name=end_date]').val()),
            },
            success: function (result) {
                primary_chart_data.labels.splice(-1 * primary_chart_data.labels.length, primary_chart_data.labels.length); // remove the label first
                $.each(result, function( key, value ) {
                    primary_chart_data.labels.push(key);
                    data.push(value);
                });
                
                let newDataset = {
                    label: 'Penjualan',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    borderWidth: 1,
                    fill: false,
                    data: data
                };
                
                primary_chart_data.datasets.pop();
                primary_chart_data.datasets.push(newDataset);
                window.primary_canvas.update();
            }
        });
    };

    function initCanvas(){
        var ctx = document.getElementById('canvas').getContext('2d');
        window.primary_canvas = new Chart(ctx, {
            type: 'line',
            data: primary_chart_data,
            options: {
                responsive: true,
                elements: {
                    line: {
                        tension: 0.000001
                    }
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return `${data.labels[tooltipItem.datasetIndex]} - ${numeral(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).format('0,0')}`;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value, index, values) {
                                return numeral(value).format('0,0');;
                            }
                        }
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        });
        changeChart();
        
    }
</script>