@include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<div class="content">
    <nav class="level">
        <div class="level-left">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>

        <div class="level-right">
        </div>
    </nav>
</div>
<div class="content">
    <nav class="level">
        <div class="level-left">
            <div class="field is-grouped">
                <div class="field">
                    <label class="label">Dari tanggal</label>
                    <div class="control">
                        <input class="input" type="text" name="start_time" value="{{Carbon\Carbon::now('Asia/Jakarta')->startOfDay()}}">
                    </div>
                </div>
                <div class="field">
                    <label class="label">Hingga</label>
                    <div class="control">
                        <input class="input" type="text" name="end_time" value="{{Carbon\Carbon::now('Asia/Jakarta')->endOfDay()}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="level-right">
            <div class="field is-grouped">
                <p class="control">
                    <a class="button is-primary" onclick="filterAction(this)">
                        Filter
                    </a>
                </p>
            </div>
        </div>
    </nav>
</div>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Gender</th>
                <th>Jumlah</th>
            </tr>
        </thead>
    </table>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/get/customer/datang?start_time=' + $('input[name=start_time]').val() + '&end_time=' + $('input[name=end_time]').val(),
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'NM_GENDER', name: 'NM_GENDER' },
            { data: 'JUMLAH_CUSTOMER', name: 'JUMLAH_CUSTOMER', searchable: false, orderable: false }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function filterAction(element){
        primary_table.ajax.url(base_url + 'api/get/customer/datang?start_time=' + $('input[name=start_time]').val() + '&end_time=' + $('input[name=end_time]').val()).load(null, false);
    };
</script>