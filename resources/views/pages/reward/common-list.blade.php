<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>

                <div class="level-right">
                    <div class="level-item">
                        <a class="button is-black is-primary-color target-link" href="dashboard#reward/add">
                            <span class="icon is-small">
                                <i class="typcn typcn-plus"></i>
                            </span>
                            <span>Tambah Reward Baru</span>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <table class="table is-hoverable is-fullwidth" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Nama</th>
                        <th>Tersedia</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/reward/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'image.image_url', searchable: false, orderable: false,
                render: function(data){
                    return '<img style="width: 75px;" src='+ base_url + data +'>';
                }
            },
            { data: 'name' },
            { data: 'available_stock' },
            { data: 'status', searchable: false, orderable: false },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a class="button is-rounded is-table is-small is-success target-link" href="dashboard#reward/edit/' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '</a>'+
                    '<a class="button is-rounded is-table is-small is-danger" onclick="deleteAction(this)" data-id="' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '</a>';
                }
            }
        ]
    });
    
    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    async function deleteAction(element) {
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Apa Anda yakin?',
            text: "Aksi ini tidak bisa dibatalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url + 'ajax/reward/delete',
                    async: false,
                    data: {
                        id: item.attr('data-id')
                    },
                    success: function (result) {
                        item.removeClass('is-loading');
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            primary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }else{
                item.removeClass('is-loading');
            }
        });
    }
</script>