@include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<div class="content">
    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
</div>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Brand</th>
                <th>Jenis</th>
                <th>Ukuran</th>
                <th>Masuk</th>
                <th>Harga Masuk</th>
                <th>Harga Jual</th>
                <th>Konsi</th>
                <th>Tanggal Masuk</th>
            </tr>
        </thead>
    </table>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/get/barang-masuk/detail?transaksi={{$transaksi_barang_masuk}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'KODE_BARANG', name: 'KODE_BARANG' },
            { data: 'NM_BRAND', name: 'NM_BRAND' },
            { data: 'NM_JENIS_BARANG', name: 'NM_JENIS_BARANG', searchable: false, orderable: false, },
            { data: 'NM_SIZE_DETAIL', name: 'NM_SIZE_DETAIL', searchable: false, orderable: false, },
            { data: 'QTY', name: 'QTY', searchable: false, orderable: false, },
            { data: 'HARGA_MASUK', name: 'HARGA_MASUK', searchable: false, orderable: false, },
            { data: 'HARGA_AKHIR', name: 'HARGA_AKHIR', searchable: false, orderable: false, },
            { data: 'KONSINYASI_BARANG', name: 'KONSINYASI_BARANG', searchable: false, orderable: false, },
            { data: 'TANGGAL_MASUK', name: 'TANGGAL_MASUK', }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

</script>