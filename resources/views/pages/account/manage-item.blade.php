<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<form id="form-manage-item" method="POST" action="{{url('ajax/account/submit')}}">
    <div class="columns">
        <div class="column is-8">
            <div class="card is-gap">
                <div class="card-content">
                    <div class="content">
                        <nav class="level">
                            <div class="level-left">
                                <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                            </div>
                        </nav>
                    </div>
                    <div class="content">
                        <input type="hidden" name="id" @if(!empty($item)) value="{{$item->account_id}}" @endif>
                        <div class="field">
                            <label class="label">Nama Akun*</label>
                            <p class="control">
                                <input class="input" type="text" name="name" required="" @if(!empty($item)) value="{{$item->name}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Email*</label>
                            <p class="control">
                                <input class="input" type="email" name="email" required="" @if(!empty($item)) value="{{$item->email}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Telepon</label>
                            <p class="control">
                                <input class="input" type="text" name="phone" @if(!empty($item)) value="{{$item->phone}}" @endif>
                            </p>
                        </div>
                         <div class="field">
                            <label class="label">Type</label>
                            <p class="control">
                                <div class="select">
                                    <select name="type" required="">
                                        <option value="admin" >admin</option>
                                        <option value="dokter" >dokter</option>
                                        <option value="customer" >customer</option>
                                    </select>
                                </div>
                            </p>
                        </div>
                        
                        {{-- <div class="field">
                            <label class="label">Tanggal Lahir*</label>
                            <p class="control">
                                <input class="input is-date" type="date" name="birthday" @if(!empty($item)) value="{{$item->birthday}}" @else value="{{Carbon\Carbon::now('Asia/Jakarta')->format('Y-m-d')}}" @endif required="">
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Gender</label>
                            <p class="control">
                                <div class="select">
                                    <select name="gender" required="">
                                        <option value="wanita" @if(!empty($item) && $item->gender == 'wanita') selected @endif>Wanita</option>
                                        <option value="pria" @if(!empty($item) && $item->gender == 'pria') selected @endif>Pria</option>
                                    </select>
                                </div>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Sudah Menikah/Belum Menikah</label>
                            <p class="control">
                                <div class="select">
                                    <select name="married_status" required="">
                                        <option value="yes" @if(!empty($item) && $item->married_status == 'yes') selected @endif>Sudah Menikah</option>
                                        <option value="no" @if(!empty($item) && $item->married_status == 'no') selected @endif>Belum Menikah</option>
                                    </select>
                                </div>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Agama*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="religion_id" required="">
                                        @foreach($religions as $religion)
                                        @if(!empty($item) && $item->religion_id == $religion->religion_id)
                                        <option value="{{$religion->religion_id}}" selected>{{$religion->name}}</option>
                                        @else
                                        <option value="{{$religion->religion_id}}">{{$religion->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Level*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="level_id" required="">
                                        @foreach($levels as $level)
                                        @if(!empty($item) && $item->level_id == $level->level_id)
                                        <option value="{{$level->level_id}}" selected>{{$level->name}}</option>
                                        @else
                                        <option value="{{$level->level_id}}">{{$level->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field is-gap">
                            <p class="subtitle">Alamat mengikuti KTP</p>
                        </div>
                        <div class="field">
                            <label class="label">Alamat</label>
                            <p class="control">
                                <textarea class="textarea" name="address_ktp"
                                    placeholder="Tulis di sini...">@if(!empty($item)) {{$item->ktp->address}} @endif</textarea>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Provinsi*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="province_ktp" required="" onchange="changeProvince('ktp')">
                                        <option value="">Pilih provinsi</option>
                                        @foreach($provinces as $province)
                                        @if(!empty($item) && $item->ktp->province_id == $province->province_id)
                                        <option value="{{$province->province_id}}" selected>{{$province->name}}</option>
                                        @else
                                        <option value="{{$province->province_id}}">{{$province->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Kota*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="city_ktp" required="" onchange="changeCity('ktp')">
                                        <option value="">Pilih kota</option>
                                        @foreach($cities as $city)
                                        @if(!empty($item) && $item->ktp->city_id == $city->city_id)
                                        <option value="{{$city->city_id}}" selected>{{$city->name}}</option>
                                        @else
                                        <option value="{{$city->city_id}}">{{$city->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Kecamatan*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="district_ktp" required="">
                                        <option value="">Pilih kecamatan</option>
                                        @foreach($districts as $district)
                                        @if(!empty($item) && $item->ktp->district_id == $district->district_id)
                                        <option value="{{$district->district_id}}" selected>{{$district->name}}</option>
                                        @else
                                        <option value="{{$district->district_id}}">{{$district->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Kode Pos</label>
                            <p class="control">
                                <input class="input" type="text" name="postal_code_ktp" @if(!empty($item)) value="{{$item->ktp->postal_code}}" @endif>
                            </p>
                        </div>
                        <div class="field is-gap">
                            <p class="subtitle">Alamat domisili</p>
                        </div>
                        <div class="field">
                            <p class="control">
                            <label class="checkbox">
                                <input type="checkbox" onchange="changeChecklist(this)"> Ceklist apabila sama dengan alamat KTP
                            </label>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Alamat</label>
                            <p class="control">
                                <textarea class="textarea" name="address_stay"
                                    placeholder="Tulis di sini...">@if(!empty($item)) {{$item->stay->address}} @endif</textarea>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Provinsi*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="province_stay" required="" onchange="changeProvince('stay')">
                                        <option value="">Pilih provinsi</option>
                                        @foreach($provinces as $province)
                                        @if(!empty($item) && $item->stay->province_id == $province->province_id)
                                        <option value="{{$province->province_id}}" selected>{{$province->name}}</option>
                                        @else
                                        <option value="{{$province->province_id}}">{{$province->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Kota*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="city_stay" required="" onchange="changeCity('stay')">
                                        <option value="">Pilih kota</option>
                                        @foreach($cities as $city)
                                        @if(!empty($item) && $item->stay->city_id == $city->city_id)
                                        <option value="{{$city->city_id}}" selected>{{$city->name}}</option>
                                        @else
                                        <option value="{{$city->city_id}}">{{$city->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Kecamatan*</label>
                            <div class="control">
                                <div class="select">
                                    <select name="district_stay" required="">
                                        <option value="">Pilih kecamatan</option>
                                        @foreach($districts as $district)
                                        @if(!empty($item) && $item->stay->district_id == $district->district_id)
                                        <option value="{{$district->district_id}}" selected>{{$district->name}}</option>
                                        @else
                                        <option value="{{$district->district_id}}">{{$district->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Kode Pos</label>
                            <p class="control">
                                <input class="input" type="text" name="postal_code_stay" @if(!empty($item)) value="{{$item->stay->postal_code}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Foto 3x4*</label>
                        </div>
                        <div class="field">
                            <div class="content">
                                <figure>
                                    @if(!empty($item) && !empty($item->photo->image_url))
                                    <img id="photo-image" src="{{url($item->photo->image_url)}}">
                                    @else
                                    <img id="photo-image" src="https://bulma.io/images/placeholders/256x256.png"> 
                                    @endif
                                </figure>
                                <input class="input" type="hidden" name="photo"> 
                            </div>
                        </div>
                        <div class="field">
                            <div class="file is-primary">
                                <label class="file-label">
                                    <input class="file-input" type="file" id="photo-file" accept="image/png, image/jpeg, image/jpg">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                            <i class="fa fa-upload"></i>
                                        </span>
                                        <span class="file-label">
                                            Pilih gambar
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Foto KTP/SIM*</label>
                        </div>
                        <div class="field">
                            <div class="content">
                                <figure>
                                    @if(!empty($item) && !empty($item->identification->image_url))
                                    <img id="identification-image" src="{{url($item->identification->image_url)}}">
                                    @else
                                    <img id="identification-image" src="https://bulma.io/images/placeholders/256x256.png"> 
                                    @endif
                                </figure>
                                <input class="input" type="hidden" name="identification"> 
                            </div>
                        </div>
                        <div class="field">
                            <div class="file is-primary">
                                <label class="file-label">
                                    <input class="file-input" type="file" id="identification-file" accept="image/png, image/jpeg, image/jpg">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                            <i class="fa fa-upload"></i>
                                        </span>
                                        <span class="file-label">
                                            Pilih gambar
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">File Scan MOU</label>
                        </div>
                        <div class="field">
                            <div class="content">
                                <figure>
                                    @if(!empty($item) && !empty($item->aggrement->image_url))
                                    <img id="aggrement-image" src="{{url($item->aggrement->image_url)}}">
                                    @else
                                    <img id="aggrement-image" src="https://bulma.io/images/placeholders/256x256.png"> 
                                    @endif
                                </figure>
                                <input class="input" type="hidden" name="aggrement"> 
                            </div>
                        </div> --}}
                        {{-- <div class="field">
                            <div class="file is-primary">
                                <label class="file-label">
                                    <input class="file-input" type="file" id="aggrement-file" accept="image/png, image/jpeg, image/jpg">
                                    <span class="file-cta">
                                        <span class="file-icon">
                                            <i class="fa fa-upload"></i>
                                        </span>
                                        <span class="file-label">
                                            Pilih gambar
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="card is-gap">
                <div class="card-content">
                    <nav class="level">
                        <div class="level-left">
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <div class="box">
                                    <div class="field is-horizontal">
                                        <div class="field body is-grouped is-grouped-right">
                                            <div class="control">
                                                <button class="button is-link is-primary-color">
                                                    <span class="icon">
                                                        <i class="fa fa-save"></i>
                                                    </span>
                                                    <span>Simpan</span>
                                                </button>
                                            </div>
                                            <div class="control">
                                                <a class="target-link" href="dashboard#account">
                                                    <button class="button is-text">Kembali</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript" src="{{asset('vendor/compressorjs/compressor.min.js')}}"></script>
<script>
    initCalendar();
    $('#form-manage-item').validate({
        highlight: function (input) {
            $(input).addClass('is-danger');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.control').addClass('help').addClass('is-danger').append(error);
        },
        submitHandler: function(form) {
            $('button').attr('disabled', 'disabled');
            $('input').attr('readonly', 'readonly');
            
            setTimeout(() => {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(result) {
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            loadURI(result.loaduri);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    },
                    complete: function() {
                        $('button').removeAttr('disabled', 'disabled');
                        $('input').removeAttr('readonly', 'readonly');
                    }
                });
                
            }, 1000);
        }
    });

    function changeProvince(type){
        $.ajax({
            type: 'POST',
            url: '{{url('ajax/account/province/change')}}',
            async: false,
            data: {
                id: $('select[name=province_'+ type +']').val()
            },
            success: function (result) {
                $('select[name=city_'+ type +']').html('');
                var html = '<option value="" selected>Pilih kota</option>';
                $.each(result.data.cities, function( key, value ) {
                    html += '<option value="'+value.city_id+'">'+value.name+'</option>'
                });
                $('select[name=city_'+ type +']').html(html);
                $('select[name=district_'+ type +']').html('<option value="" selected>Pilih kecamatan</option>');
            }
        });
    }

    function changeCity(type){
        $.ajax({
            type: 'POST',
            url: '{{url('ajax/account/city/change')}}',
            async: false,
            data: {
                id: $('select[name=city_'+ type +']').val()
            },
            success: function (result) {
                $('select[name=district_'+ type +']').html('');
                var html = '<option value="" selected>Pilih kecamatan</option>';
                $.each(result.data.districts, function( key, value ) {
                    html += '<option value="'+value.district_id+'">'+value.name+'</option>'
                });
                $('select[name=district_'+ type +']').html(html);
            }
        });
    }

    function changeChecklist(element){
        var item = $(element);
        if(item.is(':checked')){
            $('textarea[name=address_stay]').val($('textarea[name=address_ktp]').val());
            $('select[name=province_stay]').val($('select[name=province_ktp]').val());
            $('select[name=city_stay]').val($('select[name=city_ktp]').val());
            $('select[name=district_stay]').val($('select[name=district_ktp]').val());
            $('input[name=postal_code_stay]').val($('input[name=postal_code_ktp]').val());
        }else{
            $('textarea[name=address_stay]').val("");
            $('select[name=province_stay]').val("");
            $('select[name=city_stay]').val("");
            $('select[name=district_stay]').val("");
            $('input[name=postal_code_stay]').val("");
        }
    }

    document.getElementById('photo-file').addEventListener('change', (e) => {
        const file = e.target.files[0];

        if (!file) {
            return;
        }

        new Compressor(file, {
            quality: 0.6,
            success(result) {
                var reader = new FileReader();
                reader.readAsDataURL(result); 
                reader.onloadend = function() {
                    base64data = reader.result;                
                    $('input[name=photo]').val(base64data);
                    $('#photo-image').attr('src', base64data)
                }
            },
            error(err) {
                console.log(err.message);
            },
        });
    });

    document.getElementById('identification-file').addEventListener('change', (e) => {
        const file = e.target.files[0];

        if (!file) {
            return;
        }

        new Compressor(file, {
            quality: 0.6,
            success(result) {
                var reader = new FileReader();
                reader.readAsDataURL(result); 
                reader.onloadend = function() {
                    base64data = reader.result;                
                    $('input[name=identification]').val(base64data);
                    $('#identification-image').attr('src', base64data)
                }
            },
            error(err) {
                console.log(err.message);
            },
        });
    });

    document.getElementById('aggrement-file').addEventListener('change', (e) => {
        const file = e.target.files[0];

        if (!file) {
            return;
        }

        new Compressor(file, {
            quality: 0.6,
            success(result) {
                var reader = new FileReader();
                reader.readAsDataURL(result); 
                reader.onloadend = function() {
                    base64data = reader.result;                
                    $('input[name=aggrement]').val(base64data);
                    $('#aggrement-image').attr('src', base64data)
                }
            },
            error(err) {
                console.log(err.message);
            },
        });
    });
</script>