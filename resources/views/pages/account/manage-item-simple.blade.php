<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<form id="form-manage-item" method="POST" action="{{url('ajax/account/simple/submit')}}">
    <div class="columns">
        <div class="column is-8">
            <div class="card is-gap">
                <div class="card-content">
                    <div class="content">
                        <nav class="level">
                            <div class="level-left">
                                <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                            </div>
                        </nav>
                    </div>
                    <div class="content">
                        <input type="hidden" name="id" @if(!empty($item)) value="{{$item->account_id}}" @endif>
                        <div class="field">
                            <label class="label">Nama Akun*</label>
                            <p class="control">
                                <input class="input" type="text" name="name" required="" @if(!empty($item)) value="{{$item->name}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Email*</label>
                            <p class="control">
                                <input class="input" type="email" name="email" @if(!empty($item)) value="{{$item->email}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Telepon</label>
                            <p class="control">
                                <input class="input" type="text" name="phone" @if(!empty($item)) value="{{$item->phone}}" @endif>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Gender</label>
                            <p class="control">
                                <div class="select">
                                    <select name="gender" required="">
                                        <option value="wanita" @if(!empty($item) && $item->gender == 'wanita') selected @endif>Wanita</option>
                                        <option value="pria" @if(!empty($item) && $item->gender == 'pria') selected @endif>Pria</option>
                                    </select>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="card is-gap">
                <div class="card-content">
                    <nav class="level">
                        <div class="level-left">
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <div class="box">
                                    <div class="field is-horizontal">
                                        <div class="field body is-grouped is-grouped-right">
                                            <div class="control">
                                                <button class="button is-link is-primary-color">
                                                    <span class="icon">
                                                        <i class="fa fa-save"></i>
                                                    </span>
                                                    <span>Simpan</span>
                                                </button>
                                            </div>
                                            <div class="control">
                                                <a class="target-link" href="dashboard#account">
                                                    <button class="button is-text">Kembali</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    initCalendar();
    $('#form-manage-item').validate({
        highlight: function (input) {
            $(input).addClass('is-danger');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.control').addClass('help').addClass('is-danger').append(error);
        },
        submitHandler: function(form) {
            $('button').attr('disabled', 'disabled');
            $('input').attr('readonly', 'readonly');
            
            setTimeout(() => {
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function(result) {
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            loadURI(result.loaduri);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    },
                    complete: function() {
                        $('button').removeAttr('disabled', 'disabled');
                        $('input').removeAttr('readonly', 'readonly');
                    }
                });
                
            }, 1000);
        }
    });
</script>