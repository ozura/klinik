<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>

                <div class="level-right">
                @if($account->level_id == 1)
                    <div class="level-item">
                        <a class="button is-black is-primary-color target-link" href="dashboard#account/add">
                            <span class="icon is-small">
                                <i class="typcn typcn-plus"></i>
                            </span>
                            <span>Tambah Akun</span>
                        </a>
                    </div>
                @else
                    <div class="level-item">
                        <a class="button is-black is-primary-color target-link" href="dashboard#account/simple-add">
                            <span class="icon is-small">
                                <i class="typcn typcn-plus"></i>
                            </span>
                            <span>Tambah Akun</span>
                        </a>
                    </div>
                @endif
                </div>
            </nav>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <table class="table is-hoverable is-fullwidth" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        {{-- <th>Gambar</th> --}}
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Telp</th>
                        <th>Type</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div id="modal-detail-stock" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head is-primary-color">
            <p class="modal-card-title" id="modal-title">Data Stok</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="content" style="overflow-x:scroll;">
                <table class="table is-striped is-bordered is-fullwidth" id="secondary_table" style="width:100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            {{-- <th>Gambar</th> --}}
                            <th>Sku</th>
                            <th>Nama</th>
                            <th>Tersedia</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/account/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            // { data: 'photo.image_url', searchable: false, orderable: false,
            //     render: function(data){
            //         return '<img style="width: 75px;" src='+ base_url + data +'>';
            //     }
            // },
            { data: 'name' },
            { data: 'email' },
            { data: 'phone' },
            { data: 'type', searchable: false, orderable: false },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    let html = '';
                    @if($account->level_id == 1)
                    html += '<a class="button is-rounded is-table is-small is-success target-link" href="dashboard#account/edit/' +data.id+ '" data-name="' +data.name+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '</a>';
                    @else
                    if(data.level == 5){
                        html += '<a class="button is-rounded is-table is-small is-success target-link" href="dashboard#account/simple-edit/' +data.id+ '" data-name="' +data.name+ '">'+
                        '    <span class="icon">'+
                        '        <i class="typcn typcn-edit"></i>'+
                        '    </span>'+
                        '</a>';
                    }
                    @endif
                    html += '<a class="button is-rounded is-table is-small is-black target-link modal-button" onclick="showDetailAction(this)" data-target="#modal-detail-stock" data-name="' +data.name+ '" data-id="' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-eye"></i>'+
                    '    </span>'+
                    '</a>';
                    return html;
                }
            }
        ]
    });
    
    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    

    function showDetailAction(element){
        var item = $(element);
        $('#modal-title').html('Data Stok Akun '+item.attr('data-name'));
        secondary_table.ajax.url(base_url + 'ajax/account/stock/table?account_id='+item.attr('data-id')).load(null, false);
    };

</script>