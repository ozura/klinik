@include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<div class="content">
    <nav class="level">
        <div class="level-left">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>

        <div class="level-right">
            <p class="level-item">
                <a class="button is-black modal-button summary-report-btn" data-target="#modal-summary-report">
                    <span class="icon is-small">
                        <i class="typcn typcn-document"></i>
                    </span>
                    <span>Summary Report</span>
                </a>
            </p>
        </div>
    </nav>
</div>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>No Invoice</th>
                <th>Deskripsi</th>
                <th>Total Qty Masuk</th>
                <th>Total Harga Asli</th>
                <th>Total Harga Jual</th>
                <th>Tanggal Masuk</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<div id="modal-summary-report" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head is-primary-color">
            <p class="modal-card-title">Summary Report</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <table class="table is-striped is-bordered is-fullwidth">
                <tbody>
                    <tr>
                        <th>Stok masuk</th>
                        <th>Jumlah total masuk (Harga asli)</th>
                        <th>Jumlah total masuk (Harga jual)</th>
                    </tr>
                    <tr>
                        <th id="total_masuk">x pcs</th>
                        <th id="total_hm_masuk">Rp</th>
                        <th id="total_hj_masuk">Rp</th>
                    </tr>
                </tbody>
            </table>
            <table class="table is-striped is-bordered is-fullwidth">
                <tbody>
                    <tr>
                        <th>Stok terjual</th>
                        <th>Jumlah total terjual (Harga asli)</th>
                        <th>Jumlah total terjual (Harga jual)</th>
                    </tr>
                    <tr>
                        <th id="total_terjual">x pcs</th>
                        <th id="total_hm_terjual">Rp</th>
                        <th id="total_hj_terjual">Rp</th>
                    </tr>
                </tbody>
            </table>
            <table class="table is-striped is-bordered is-fullwidth">
                <tbody>
                    <tr>
                        <th>Stok tersedia</th>
                        <th>Jumlah total tersedia (Harga asli)</th>
                        <th>Jumlah total tersedia (Harga jual)</th>
                    </tr>
                    <tr>
                        <th id="total_tersedia">x pcs</th>
                        <th id="total_hm_tersedia">Rp</th>
                        <th id="total_hj_tersedia">Rp</th>
                    </tr>
                </tbody>
            </table>
        </section>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/get/barang-masuk',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'NO_INVOICE', name: 'NO_INVOICE' },
            { data: 'DESKRIPSI', name: 'DESKRIPSI' },
            { data: 'TOTAL_QTY', name: 'TOTAL_QTY', searchable: false, orderable: false, },
            { data: 'TOTAL_HARGA_MASUK', name: 'TOTAL_HARGA_MASUK', searchable: false, orderable: false, },
            { data: 'TOTAL_HARGA_AKHIR', name: 'TOTAL_HARGA_AKHIR', searchable: false, orderable: false, },
            { data: 'TANGGAL_MASUK', name: 'TANGGAL_MASUK', },
            { data: 'action', name: 'action', searchable: false, orderable: false, 
                render: function(data){
                    return '<a class="button is-small is-rounded is-black is-primary-color target-link" href="dashboard#barang-masuk/detail/'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-eye"></i>'+
                    '    </span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    $(document).on('click', '.summary-report-btn', function () {
        $.ajax({
            type: 'POST',
            url: base_url + 'api/summary',
            success: function (result) {
                $('#total_masuk').html(result.summary_barang_masuk.total_stok);
                $('#total_hm_masuk').html(result.summary_barang_masuk.total_harga_masuk);
                $('#total_hj_masuk').html(result.summary_barang_masuk.total_harga_jual);

                $('#total_terjual').html(result.summary_barang_terjual.total_stok);
                $('#total_hm_terjual').html(result.summary_barang_terjual.total_harga_masuk);
                $('#total_hj_terjual').html(result.summary_barang_terjual.total_harga_jual);

                $('#total_tersedia').html(result.summary_barang_tersedia.total_stok);
                $('#total_hm_tersedia').html(result.summary_barang_tersedia.total_harga_masuk);
                $('#total_hj_tersedia').html(result.summary_barang_tersedia.total_harga_jual);
            }
        });
    });

</script>