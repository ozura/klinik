<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>
            </nav>
        </div>
        <div class="content">
            <article class="media">
                <figure class="media-left">
                    <p class="image is-64x64">
                        <img src="{{asset($account->photo->image_url)}}">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="content">
                        <p>
                            <strong>Poin Saya</strong>
                            <br> <small>{{$account->points}} poin</small>
                        </p>
                    </div>
                </div>
                <div class="media-right">
                    <p class="buttons">
                        <a class="button is-link target-link" href="dashboard#points/exchanged">
                            <span class="icon">
                                <i class="typcn typcn-gift"></i>
                            </span>
                            <span>Reward saya</span>
                        </a>
                        <a class="button is-warning target-link" href="dashboard#points/detail">
                            <span class="icon">
                                <i class="typcn typcn-point-of-interest"></i>
                            </span>
                            <span>History</span>
                        </a>
                    </p>
                </div>
            </article>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <p class="subtitle">Reward tukar poin</p>
            <table class="table is-hoverable is-fullwidth cards" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Nama</th>
                        <th>Tersedia</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        bLengthChange: false,
        bFilter: false,
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/reward/table',
            type: 'POST'
        },
        columns: [
            { data: 'image.image_url', searchable: false, orderable: false,
                render: function(data){
                    return '<img style="width: 75px;" src='+ base_url + data +'>';
                }
            },
            { data: 'name' },
            { data: 'change_points' },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a class="button is-rounded is-table is-small is-success" onclick="changeAction(this)" data-id="' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-flow-switch"></i>'+
                    '    </span>'+
                    '    <span>Tukar poin</span>'+
                    '</a>';
                }
            }
        ]
    });

    async function changeAction(element) {
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Apa Anda yakin?',
            text: "Aksi ini tidak bisa dibatalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url + 'ajax/reward/change',
                    async: false,
                    data: {
                        id: item.attr('data-id')
                    },
                    success: function (result) {
                        item.removeClass('is-loading');
                        if(result.status_code == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            primary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }else{
                item.removeClass('is-loading');
            }
        });
    }

</script>