<div class="card">
    <div class="card-content">
        @include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
    </div>
</div>
<div class="card is-gap">
    <div class="card-content">
        <div class="content">
            <nav class="level">
                <div class="level-left">
                    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
                </div>
            </nav>
        </div>
        <div class="content" style="overflow-x:scroll;">
            <table class="table is-hoverable is-fullwidth" id="primary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th>Ditukar pada</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'ajax/points/exchanged/table',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'reward.image.image_url', searchable: false, orderable: false,
                render: function(data){
                    return '<img style="width: 75px;" src='+ base_url + data +'>';
                }
            },
            { data: 'reward.name' },
            { data: 'status', searchable: false, orderable: false },
            { data: 'created_at' },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a class="button is-rounded is-table is-small is-black target-link" href="dashboard#reward/detail/' +data.id+ '">'+
                    '    <span class="icon">'+
                    '        <i class="typcn typcn-eye"></i>'+
                    '    </span>'+
                    '</a>';
                }
            }
        ]
    });
    
    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();
</script>