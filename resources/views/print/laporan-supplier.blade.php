<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,600">
<link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-0.6.2/css/bulma.css')}}">
</head>
<style>
    table th {
        font-size: 9px;
    }
    table td {
        font-size: 9px;
    }
</style>
<body>
    <div class="content has-text-right">
        <p class="title is-6">Laporan Supplier</p>
    </div>
    <div class="content has-text-centered">
        <p class="title is-4">Brand: {{$nama_brand}}</p>
        <p class="subtitle is-6">Dari tanggal: {{$start_date}}
        <br>Sampai tanggal: {{$end_date}}</p>
        @php
            $total_qty = 0;
            $total_transaksi = 0;
            $total_qty_reguler = 0;
            $total_transaksi_reguler = 0;
            $total_qty_sale = 0;
            $total_profit_supplier = 0;
        @endphp
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Brand</th>
                    <th>Kode barang</th>
                    <th>Size</th>
                    <th>Jenis barang</th>
                    <th>Harga Masuk</th>
                    <th>Harga Jual</th>
                    <th>Qty</th>
                    <th>Konsiyansi</th>
                    <th>Profit Supplier</th>
                    <th>Tanggal Transaksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($list_data as $no => $data)
                @if($data->IS_DISKON == 1 || $data->IS_BUY1_GET1 == 1)
                    @php
                    $total_qty_sale += $data->QTY;
                    $total_transaksi_sale += $data->QTY * $data->HARGA_AKHIR;
                    $status = 'SALE';
                    @endphp
                    @if($data->HARGA_AKHIR > 0 && $data->QTY > 0)
                        @php
                            $konsinyasi = $data->PROFIT_BERSIH / $data->HARGA_AKHIR / $data->QTY * 100;
                        @endphp
                    @else
                        @php
                            $konsinyasi = 0;
                        @endphp
                    @endif
                @else
                    @php
                    $total_qty_reguler += $data->QTY;
                    $total_transaksi_reguler += $data->QTY * $data->HARGA_AKHIR;
                    $status = 'REGULER';
                    @endphp
                    @if($data->HARGA_MASUK > 0 && $data->QTY > 0)
                        @php
                            $konsinyasi = $data->PROFIT_BERSIH / $data->HARGA_MASUK / $data->QTY * 100;
                        @endphp
                    @else
                        @php
                            $konsinyasi = 0;
                        @endphp
                    @endif
                @endif
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{$data->NM_BRAND}}</td>
                    <td>{{$data->KODE_BARANG}}</td>
                    <td>{{$data->NM_SIZE_DETAIL}}</td>
                    <td>{{$data->NM_JENIS_BARANG}}</td>
                    <td>{{'Rp'.number_format($data->HARGA_MASUK)}}</td>
                    <td>{{'Rp'.number_format($data->HARGA_AKHIR)}}</td>
                    <td>{{$data->QTY.' pcs'}}</td>
                    <td>{{$konsinyasi.'%'}}</td>
                    <td>{{'Rp'.number_format($data->PROFIT_SUPPLIER)}}</td>
                    <td>{{$data->TANGGAL}}</td>
                </tr>
                @php
                $total_qty += $data->QTY;
                $total_transaksi += $data->QTY * $data->HARGA_AKHIR;
                $total_profit_supplier += $data->PROFIT_SUPPLIER;
                @endphp
            @endforeach
            </tbody>
        </table>
        <div class="columns">
            <div class="column">
                <table class="table" style="width: 25%;">
                    <tbody>
                        <tr>
                            <th>Terjual</th>
                            <td>{{$total_qty.' pcs'}}</td>
                        </tr>
                        <tr>
                            <th>Total Transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi)}}</td>
                        </tr>
                        <tr>
                            <th>Bayar Supplier</th>
                            <td>{{'Rp'.number_format($total_profit_supplier)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="column">
            </div>
            @if($status_laporan == 2 || $status_laporan == 3)
            <div class="column">
                <table class="table" style="width: 25%;">
                    <p>Barang SALE</p>
                    <tbody>
                        <tr>
                            <th>Terjual</th>
                            <td>{{$total_qty_sale.' pcs'}}</td>
                        </tr>
                        <tr>
                            <th>Total Transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi_sale)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @endif
            @if($status_laporan == 1 || $status_laporan == 3)
            <div class="column">
                <table class="table" style="width: 25%;">
                    <p>Barang REGULER</p>
                    <tbody>
                        <tr>
                            <th>Terjual</th>
                            <td>{{$total_qty_reguler.' pcs'}}</td>
                        </tr>
                        <tr>
                            <th>Total Transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi_reguler)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</body>

</html>