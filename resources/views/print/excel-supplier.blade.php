<table>
    <thead>
        <tr>
            <th>No</th>
            <th>No Transaksi</th>
            <th>Brand</th>
            <th>Kode barang</th>
            <th>Size</th>
            <th>Jenis barang</th>
            <th>Status</th>
            <th>Harga Masuk</th>
            <th>Harga Jual</th>
            <th>Qty</th>
            <th>Subtotal (Harga jual x qty)</th>
            <th>Konsiyansi</th>
            <th>Profit Supplier</th>
            <th>Tanggal Transaksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach($list_data as $no => $data)
        @if($data->IS_DISKON == 1 || $data->IS_BUY1_GET1 == 1)
            @php
            $status = 'SALE';
            @endphp

            @if($data->HARGA_AKHIR > 0 && $data->QTY > 0)
                @php
                    $konsinyasi = $data->PROFIT_BERSIH / $data->HARGA_AKHIR / $data->QTY * 100;
                @endphp
            @else
                @php
                    $konsinyasi = 0;
                @endphp
            @endif
        @else
            @php
            $status = 'REGULER';
            @endphp
            @if($data->HARGA_MASUK > 0 && $data->QTY > 0)
                @php
                    $konsinyasi = $data->PROFIT_BERSIH / $data->HARGA_MASUK / $data->QTY * 100;
                @endphp
            @else
                @php
                    $konsinyasi = 0;
                @endphp
            @endif
        @endif
        <tr>
            <td>{{$no+1}}</td>
            <td>{{$data->NO_TRANSAKSI}}</td>
            <td>{{$data->NM_BRAND}}</td>
            <td>{{$data->KODE_BARANG}}</td>
            <td>{{$data->NM_SIZE_DETAIL}}</td>
            <td>{{$data->NM_JENIS_BARANG}}</td>
            <td>{{$status}}</td>
            <td>{{$data->HARGA_MASUK}}</td>
            <td>{{$data->HARGA_AKHIR}}</td>
            <td>{{$data->QTY}}</td>
            <td>{{$data->HARGA_AKHIR * $data->QTY}}</td>
            <td>{{$konsinyasi}}</td>
            <td>{{$data->PROFIT_SUPPLIER}}</td>
            <td>{{$data->TANGGAL}}</td>
        </tr>
    @endforeach
    </tbody>
</table>