<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,600">
<link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-0.6.2/css/bulma.css')}}">
</head>
<style>
    table th {
        font-size: 9px;
    }
    table td {
        font-size: 9px;
    }
</style>
<body>
    <div class="content has-text-right">
        <p class="title is-6">Laporan Toko</p>
    </div>
    <div class="content has-text-centered">
        <p class="title is-4">Brand: {{$nama_brand}}</p>
        <p class="subtitle is-6">Dari tanggal: {{$start_date}}
        <br>Sampai tanggal: {{$end_date}}</p>
        @php
            $total_qty = 0;
            $total_transaksi = 0;
            $total_profit_bersih = 0;
            $total_profit_supplier = 0;
            $total_potongan = 0;

            $total_qty_reguler = 0;
            $total_qty_sale = 0;
            $total_transaksi_reguler = 0;
            $total_transaksi_sale = 0;

            $temp_no_transaksi = '';
        @endphp
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Transaksi</th>
                    <th>Pembayaran Melalui</th>
                    <th>Brand</th>
                    <th>Kode barang</th>
                    <th>Size</th>
                    <th>Jenis barang</th>
                    <th>Harga Masuk</th>
                    <th>Harga Jual</th>
                    <th>Qty</th>
                    <th>Tanggal Transaksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($list_data as $no => $data)
                @if($data->IS_KENA_VOUCHER == 1)
                <tr style="background-color: #E91E63; color:white;">
                @elseif($data->IS_FROM_MUTASI == 1)
                <tr style="background-color: #00d1b2; color:white;">
                @else
                <tr>
                @endif
                    <td>{{$no+1}}</td>
                    <td>{{$data->NO_TRANSAKSI}}</td>
                    <td>{{$data->METODE_PEMBAYARAN}}</td>
                    <td>{{$data->NM_BRAND}}</td>
                    @if($data->IS_KENA_VOUCHER == 1)
                    <td>{{$data->KODE_BARANG}} <br> {{$data->NM_VOUCHER}} 
                        @if($temp_no_transaksi != $data->NO_TRANSAKSI)
                        <br> Diskon Rp{{number_format($data->POTONGAN)}}</td>
                        @endif
                    @else
                    <td>{{$data->KODE_BARANG}}</td>
                    @endif
                    <td>{{$data->NM_SIZE_DETAIL}}</td>
                    <td>{{$data->NM_JENIS_BARANG}}</td>
                    <td>{{'Rp'.number_format($data->HARGA_MASUK_BARANG)}}</td>
                    <td>{{'Rp'.number_format($data->HARGA_AKHIR)}}</td>
                    <td>{{$data->QTY.' pcs'}}</td>
                    <td>{{$data->TANGGAL}}</td>
                </tr>
                @php
                $total_qty += $data->QTY;
                $total_transaksi += $data->QTY * $data->HARGA_AKHIR;

                if($temp_no_transaksi != $data->NO_TRANSAKSI){
                    if($data->IS_KENA_VOUCHER == 1){
                        $total_potongan += $data->POTONGAN;
                    }
                }

                $temp_no_transaksi = $data->NO_TRANSAKSI;

                if($data->IS_KENA_VOUCHER == 1 || !empty($data->ID_DISKON_DETAIL)){
                    $total_qty_sale += $data->QTY;
                    $total_transaksi_sale += $data->QTY * $data->HARGA_AKHIR;
                }else{
                    $total_qty_reguler += $data->QTY;
                    $total_transaksi_reguler += $data->QTY * $data->HARGA_AKHIR;
                }
                @endphp
            @endforeach
            </tbody>
        </table>
        <div class="columns">
            <div class="column">
                <table class="table" style="width: 25%;">
                    <tbody>
                        <tr>
                            <th>Terjual</th>
                            <td>{{$total_qty.' pcs'}}</td>
                        </tr>
                        <tr>
                            <th>Subtotal transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi)}}</td>
                        </tr>
                        <tr>
                            <th>Potongan</th>
                            <td>{{'Rp'.number_format($total_potongan)}}</td>
                        </tr>
                        <tr>
                            <th>Total Transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi - $total_potongan)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="column">
            </div>
            <div class="column">
                BARANG REGULER
                <table class="table" style="width: 25%;">
                    <tbody>
                        <tr>
                            <th>Terjual</th>
                            <td>{{$total_qty_reguler.' pcs'}}</td>
                        </tr>
                        <tr>
                            <th>Total Transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi_reguler)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="column">
                BARANG DISKON
                <table class="table" style="width: 25%;">
                    <tbody>
                        <tr>
                            <th>Terjual</th>
                            <td>{{$total_qty_sale.' pcs'}}</td>
                        </tr>
                        <tr>
                            <th>Subtotal transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi_sale)}}</td>
                        </tr>
                        <tr>
                            <th>Potongan</th>
                            <td>{{'Rp'.number_format($total_potongan)}}</td>
                        </tr>
                        <tr>
                            <th>Total Transaksi</th>
                            <td>{{'Rp'.number_format($total_transaksi_sale - $total_potongan)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>