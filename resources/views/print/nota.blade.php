<!DOCTYPE html>
<html>
<style>
    ptext {
        padding: 0;
        font-size: 3mm;
        display: block;
    }

    @page {
        margin: 0px;
    }

    body {
        margin: 0px;
        margin-right: 5.5mm;
    }
</style>

<body>
    <ptext>
        <ptext style="text-align: center">
            {{substr($transaction->transaction_number, 0, 2)}}-{{substr($transaction->transaction_number, 2, 4)}}-{{substr($transaction->transaction_number, 6, 2)}}-{{substr($transaction->transaction_number, 8, 2)}}-{{substr($transaction->transaction_number, 10, 2)}}-{{substr($transaction->transaction_number, 12)}}
            <br>
            <br> Violetta
            <br> Surabaya
            <br> Tanggal {{date_format(date_create($transaction->created_at), 'd M Y H:i')}}
            <br> Penjual | {{$transaction->seller->name}}
            @if(!empty($transaction->customer_id))
            <br> Customer | {{$transaction->customer->name}}
            @endif
            <br> --------------------------------------------
        </ptext>
        <br> 
        <ptext style="margin-right: 4mm">
            @foreach($transaction_details as $transaction_detail) 
            {{$transaction_detail->product->sku}} | {{$transaction_detail->product->name}} x{{$transaction_detail->quantity}}
            <br>
            <ptext style="text-align: right">
                {{'@Rp'.number_format($transaction_detail->price, 0, ',', '.')}}
                <br>
            </ptext>
            <br>
            @endforeach
            <ptext style="text-align: right">
                ({{$transaction_details->sum('quantity')}} ITEM) TOTAL {{'Rp'.number_format($transaction->subtotal)}}
                <br>
            </ptext>
        </ptext>
        <ptext style="text-align: center">
            --------------------------------------------
            <br> Terima kasih telah berbelanja :)
            <br> --------------------------------------------
            <br>
        </ptext>
    </ptext>
</body>

</html>