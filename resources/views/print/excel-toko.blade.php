<table>
    <thead>
        <tr>
            <th>No</th>
            <th>No Transaksi</th>
            <th>Brand</th>
            <th>Kode barang</th>
            <th>Size</th>
            <th>Jenis barang</th>
            <th>Harga Masuk</th>
            <th>Harga Jual</th>
            <th>Qty</th>
            <th>Subtotal (Harga jual x qty)</th>
            <th>Profit Bersih</th>
            <th>Profit Supplier</th>
            <th>Tanggal Transaksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach($list_data as $no => $data)
        <tr>
            <td>{{$no+1}}</td>
            <td>{{$data->NO_TRANSAKSI}}</td>
            <td>{{$data->NM_BRAND}}</td>
            <td>{{$data->KODE_BARANG}}</td>
            <td>{{$data->NM_SIZE_DETAIL}}</td>
            <td>{{$data->NM_JENIS_BARANG}}</td>
            <td>{{$data->HARGA_MASUK}}</td>
            <td>{{$data->HARGA_AKHIR}}</td>
            <td>{{$data->QTY}}</td>
            <td>{{$data->HARGA_AKHIR * $data->QTY}}</td>
            <td>{{$data->PROFIT_BERSIH}}</td>
            <td>{{$data->PROFIT_SUPPLIER}}</td>
            <td>{{$data->TANGGAL}}</td>
        </tr>
    @endforeach
    </tbody>
</table>