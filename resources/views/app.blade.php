<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
        <meta name="token" content="{{csrf_token()}}">
        <meta name="theme-color" content="#000000">

        <title>Violetta | agent management system</title>
        <link rel="shortcut icon" href="{{asset('images/favicon-32x32.png')}}">
        <link rel="manifest" href="{{asset('manifest.json')}}">
        
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <base href="{{url('/')}}">

        <meta name="ogTitle" property="og:title" content="Violetta | agent management system">
        <meta name="ogDescription" property="og:description" content="">
        <meta name="ogImage" property="og:image" content="{{asset('images/logo.png')}}">
        <meta name="ogUrl" property="og:url" content="{{url('/')}}">

        <meta name="apple-mobile-web-app-title" content="Violetta | agent management system">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <link rel="apple-touch-icon" href="{{asset('images/favicon-32x32.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/favicon-32x32.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/favicon-32x32.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('images/favicon-32x32.png')}}">

        @stack('meta')
        
        <!-- external src -->
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,600">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css">
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
        
        <!-- internal src -->
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/sweetalert2-7.18.0/dist/sweetalert2.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/izitoast-1.3.0/dist/css/iziToast.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/nprogress-0.2.0/nprogress.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-flatly-0.7.2/bulma.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-quickview/dist/bulma-quickview.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-calendar/dist/css/bulma-calendar.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/style.css?v=82')}}">

        <style>
            
        </style>
        <script>
            var base_url = document.getElementsByTagName('base')[0].getAttribute('href') + '/';
        </script>
    </head>
    <body>
        @yield('content')
    </body>
    
    <!-- external src -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js" integrity="sha256-ihAoc6M/JPfrIiIeayPE9xjin4UWjsx2mjW/rtmxLM4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/autonumeric@4.5.4/dist/autoNumeric.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
    <script type="text/javascript" src="https://www.chartjs.org/dist/2.8.0/Chart.min.js"></script>

    <!-- internal src -->
    <script type="text/javascript" src="{{asset('vendor/numeral-2.0.6.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/sweetalert2-7.18.0/dist/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/izitoast-1.3.0/dist/js/iziToast.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/nprogress-0.2.0//nprogress.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/bulma-quickview/dist/bulma-quickview.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/bulma-calendar/dist/js/bulma-calendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/printjs-1.0.40/print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/jquery-validation-1.19.0/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app-bulma.js?v=64')}}"></script>
    @stack('scripts')
    <script>
        
    @if(Session::has('toast-error'))
        $(window).load(function(){
            iziToast.warning({ title: 'Oops', message: '{{Session::get('toast-error')}}', position: 'topRight' });
        });
    @endif
    @if(Session::has('toast-success'))
        $(window).load(function(){
            iziToast.success({ title: 'Good job', message: '{{Session::get('toast-success')}}', position: 'topRight' });
        });
    @endif
    @if(Session::has('swal-error'))
        $(window).load(function(){
            swal('Oops', '{{Session::get('swal-error')}}', 'error');
        });
    @endif
    @if(Session::has('swal-success'))
        $(window).load(function(){
            swal('Good job', '{{Session::get('swal-success')}}', 'success');
        });
    @endif
    function initAutoNumeric () {
        const anElementCurrency = AutoNumeric.multiple('.is-currency', {
            currencySymbol: "Rp",
            decimalCharacter: ",",
            decimalPlaces: 0,
            digitGroupSeparator: ".",
            unformatOnSubmit: true,
            unformatOnHover: false
        });

        const anElementNumber = AutoNumeric.multiple('.is-number', {
            decimalCharacter: ",",
            decimalPlaces: 0,
            digitGroupSeparator: ".",
            unformatOnSubmit: true,
            unformatOnHover: false
        });
    };
    function initCalendar () {
        const datetimeCalendars = bulmaCalendar.attach('.is-datepicker', {
            displayMode: 'dialog',
            dateFormat: 'YYYY-MM-DD',
            validateLabel: 'Select'
        });
        const dateCalendars = bulmaCalendar.attach('.is-date', {
            displayMode: 'dialog',
            dateFormat: 'YYYY-MM-DD'
        });
    };
    </script>
</html>
