<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('register', 'AccountController@actionRegister');

/* Admin User */
Route::get('/', 'AccountController@indexSignIn');
Route::post('signin', 'AccountController@actionSignIn');
Route::get('signout', 'AccountController@actionSignOut');

Route::group(['middleware' => ['my.auth']], function () {
    Route::get('dashboard', 'AccountController@indexDashboard');
    Route::get('welcome', 'AccountController@indexWelcome');
    
    Route::group(['prefix' => 'account'], function(){
        Route::get('/', 'AccountController@indexList');
        Route::get('add', 'AccountController@indexManageItem');
        Route::get('edit/{id}', 'AccountController@indexManageItem');
        Route::get('simple-add', 'AccountController@indexManageItem');
        Route::get('simple-edit/{id}', 'AccountController@indexManageItem');
    });

    Route::group(['prefix' => 'cart'], function(){
        Route::get('/', 'CartController@indexList');
    });

    Route::group(['prefix' => 'file'], function(){
        Route::get('/', 'FileController@indexList');
        Route::get('add', 'FileController@indexManageItem');
        Route::get('edit/{id}', 'FileController@indexManageItem');
    });

    Route::group(['prefix' => 'points'], function(){
        Route::get('/', 'PointsController@indexList');
        Route::get('exchanged', 'PointsController@indexExchanged');
        Route::get('detail', 'PointsController@indexDetail');
    });
    
    Route::group(['prefix' => 'post'], function(){
        Route::get('/', 'PostController@indexList');
        Route::get('add', 'PostController@indexManageItem');
        Route::get('edit/{id}', 'PostController@indexManageItem');
        Route::get('detail/{id}', 'PostController@indexDetail');
    });

    Route::group(['prefix' => 'product'], function(){
        Route::get('/', 'ProductController@indexList');
        Route::get('add', 'ProductController@indexManageItem');
        Route::get('edit/{id}', 'ProductController@indexManageItem');
    });

    Route::group(['prefix' => 'reward'], function(){
        Route::get('/', 'RewardController@indexList');
        Route::get('add', 'RewardController@indexManageItem');
        Route::get('edit/{id}', 'RewardController@indexManageItem');
    });

    Route::group(['prefix' => 'setting'], function(){
        Route::get('/', 'SettingController@indexManage');
    });

    Route::group(['prefix' => 'transaction'], function(){
        Route::get('/', 'TransactionController@indexList');
        Route::get('print', 'TransactionController@indexNotaPrint');
    });
    Route::group(['prefix' => 'tdokter'], function(){
        Route::get('/', 'TDokterController@indexList');
        Route::get('print', 'TDokterController@indexNotaPrint');
    });
    Route::group(['prefix' => 'tjasa'], function(){
        Route::get('/', 'TJasaController@indexList');
        Route::get('print', 'TJasaController@indexNotaPrint');
    });

    Route::group(['prefix' => 'ajax'], function(){
        Route::group(['prefix' => 'account'], function(){
            Route::post('province/change', 'AddressController@actionChangeProvince');
            Route::post('city/change', 'AddressController@actionChangeCity');

            Route::post('submit', 'AccountController@actionSave');
            Route::post('simple/submit', 'AccountController@actionSimpleSave');
            Route::post('table', 'AccountController@commonList');
            Route::post('stock/table', 'ProductController@stockByAccountList');
        });

        Route::group(['prefix' => 'product'], function(){
            Route::post('submit', 'ProductController@actionSave');
            Route::post('table', 'ProductController@commonList');
            Route::post('stock/save', 'ProductController@actionStockSave');
        });

        Route::group(['prefix' => 'post'], function(){
            Route::post('submit', 'PostController@actionSave');
            Route::post('table', 'PostController@commonList');
            Route::post('delete', 'PostController@actionDelete');

            Route::post('summernote/upload', 'PostController@actionUploadImage');
        });

        Route::group(['prefix' => 'cart'], function(){
            Route::post('checkout', 'CartController@actionCheckout');
            Route::post('add', 'CartController@actionAddCart');
            Route::post('add-quantity', 'CartController@actionAddQuantiyCart');
            Route::post('delete', 'CartController@actionDelete');
            Route::post('customer/add', 'CartController@actionCustomerAdd');
            Route::post('customer/delete', 'CartController@actionCustomerDelete');
            Route::post('table', 'CartController@commonList');
        });

        Route::group(['prefix' => 'customer'], function(){
            Route::post('table', 'AccountController@customerList');
        });

        Route::group(['prefix' => 'file'], function(){
            Route::post('submit', 'FileController@actionSave');
            Route::post('table', 'FileController@commonList');
            Route::post('delete', 'FileController@actionDelete');
        });

        Route::post('password/change', 'AccountController@actionChangePassword');
        
        Route::group(['prefix' => 'points'], function(){
            Route::post('table', 'PointsController@commonList');
            Route::post('exchanged/table', 'PointsController@exchangedList');
        });

        Route::group(['prefix' => 'reward'], function(){
            Route::post('change', 'RewardController@actionExchanged');
            Route::post('submit', 'RewardController@actionSave');
            Route::post('table', 'RewardController@commonList');
            Route::post('delete', 'RewardController@actionDelete');
        });

        Route::group(['prefix' => 'transaction'], function(){
            Route::post('table', 'TransactionController@commonList');
            Route::post('detail/table', 'TransactionController@detailList');
            Route::post('chart', 'TransactionController@chartList');
            Route::post('product/chart', 'TransactionController@productChartList');
        });

        Route::post('setting/submit', 'SettingController@actionSave');
    });
});