<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;


class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $request->request->add(['account' => Auth::user()]);
            return $next($request);
        }
        
        return redirect('signout');
    }
}
