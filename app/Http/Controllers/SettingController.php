<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

use App\Models\Setting;

use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class SettingController extends BaseController{
    
    public function indexManage(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Setting', 'link' => 'setting']
        );

        $settings = Setting::get();

        return view('pages/manage-setting', compact('breadcrumb', 'settings'));
    }

    /* API */
    public function actionSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'points_get' => 'required',
            'to_get_points' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        DB::beginTransaction();

        try {
            if($item = Setting::where('name', 'to_get_points')->first()){
                $item->content = $input->to_get_points;
                $item->save();    
            }

            if($item = Setting::where('name', 'points_get')->first()){
                $item->content = $input->points_get;
                $item->save();    
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

}