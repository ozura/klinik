<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;


use App\Libraries\HelperFunction;

use App\Models\Image;
use App\Models\Level;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductPurchasePrice;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class ProductController extends BaseController{
    
    public function indexList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Barang', 'link' => 'product']
        );

        return view('pages/product/common-list', compact('breadcrumb', 'account'));
    }

    public function indexManageItem(Request $request, $id = 0){
        if($item = Product::find($id)){
            $product_purchase_prices = ProductPurchasePrice::where('product_id', $id)->get();
            $breadcrumb_item = (object) array(
                'name' => 'Edit Barang', 
                'link' => 'product/edit/' .$id
            );
        }else{
            $item = null;
            $product_purchase_prices = null;
            $breadcrumb_item = (object) array(
                'name' => 'Tambah Barang', 
                'link' => 'product/add'
            );
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Barang', 'link' => 'product'],
            $breadcrumb_item
        );

        $levels = Level::orderBy('level_id', 'asc')->get();

        return view('pages/product/manage-item', compact('breadcrumb', 'item', 'levels', 'product_purchase_prices'));
    }

    /* API */
    public function commonList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;
        $list_data =Product::all();
        // $list_data = ProductStock::where('account_id', $account->account_id)->with('product', 'product.image', 'product.purchase_prices', 'product.purchase_prices.level');

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->product_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function stockByAccountList(Request $request){
        $input = (object) $request->input();

        if(empty($input->account_id)){
            $list_data = array();
        }else{
            $list_data = ProductStock::where('account_id', $input->account_id)->with('product', 'product.image');
        }

        return Datatables::of($list_data)
                ->editColumn('final_stock', function($item){
                    return $item->final_stock.' pcs';
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();


        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'name' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = $input->account;
        
        DB::beginTransaction();

        try {
            if(empty($input->id)){
                $item = new Product;
                $loaduri = 'product';
            }else{
                if($item = Product::find($input->id)){
                    // Next
                    $loaduri = 'product/edit/'.$input->id;
                }
            }

            // if(!empty($request->input('image'))){
            //     $file = $request->input('image');
            //     $tanggal = Carbon::today()->format('Y-m-d');

            //     $image_helper = HelperFunction::uploadBase64File($file, $tanggal);
    
            //     $image = new Image;
            //     $image->name = 'Cover produk '.$input->name;
            //     $image->type = $image_helper->ext;
            //     $image->image_url = $image_helper->path;
            //     $image->save();

            //     $item->image_id = $image->image_id;
            // }
    
            $item->name = $input->name;
            $item->harga = $input->harga;
            $item->type = $input->type;
            $item->persen = $input->persen;
            $item->save();

            // if($product_stock = ProductStock::where(['product_id' => $item->product_id, 'account_id' => $account->account_id])->first()){
            //     // Next
            // }else{
            //     $product_stock = new ProductStock;
            //     $product_stock->account_id = $account->account_id;
            //     $product_stock->product_id = $item->product_id;
            //     $product_stock->save();
            // }

            // $levels = $input->levels;
            // $prices = $input->prices;
            // foreach($levels as $i => $level){
            //     if($product_purchase_price = ProductPurchasePrice::where(['product_id' => $item->product_id, 'level_id' => $level])->first()){
            //         // Next
            //     }else{
            //         $product_purchase_price = new ProductPurchasePrice;
            //         $product_purchase_price->product_id = $item->product_id;
            //         $product_purchase_price->level_id = $level;
            //     }
            //     $product_purchase_price->price = $prices[$i];
            //     $product_purchase_price->save();
            // }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionStockSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'stock' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = $input->account;
        
        DB::beginTransaction();

        try {
            if($item = Product::find($input->id)){
                if($product_stock = ProductStock::where(['product_id' => $item->product_id, 'account_id' => $account->account_id])->first()){
                    if($product_stock->final_stock >= 0){
                        $product_stock->final_stock = $input->stock;
                        $product_stock->save();
                    }
                }
            }
    
            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

}