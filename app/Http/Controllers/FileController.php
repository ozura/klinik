<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Libraries\HelperFunction;

use App\Models\File;
use Yajra\Datatables\Datatables;

use Carbon\Carbon;

use Auth;
use DB;
use Session;
use Validator;

class FileController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data File', 'link' => 'file']
        );

        return view('pages/file/common-list', compact('breadcrumb'));
    }

    public function indexManageItem(Request $request, $id = 0){
        if($item = File::find($id)){
            $breadcrumb_item = (object) array(
                'name' => 'Edit File', 
                'link' => 'file/edit/' .$id
            );
        }else{
            $item = null;
            $breadcrumb_item = (object) array(
                'name' => 'Tambah File', 
                'link' => 'file/add'
            );
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data File', 'link' => 'files'],
            $breadcrumb_item
        );

        return view('pages/file/manage-item', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        $list_data = File::orderBy('name');
        
        return Datatables::of($list_data)
                ->editColumn('name', function($item){
                    $data = array(
                        'name' => $item->name,
                        'file_url' => $item->file_url
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->file_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = $input->account;
        
        DB::beginTransaction();

        try {
            if(empty($input->id)){
                $item = new File;
                $loaduri = 'file';
            }else{
                if($item = File::find($input->id)){
                    // Next
                    $loaduri = 'file/edit/'.$input->id;
                }
            }


            if ($request->hasFile('file_base')) {
                $tanggal = Carbon::today()->format('Y-m-d');
                $file_helper = HelperFunction::uploadFile($request->file('file_base'), $tanggal);

                $item->name = $input->name;
                $item->type = $file_helper->ext;
                $item->file_url = $file_helper->path;
                $item->save();
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        DB::beginTransaction();

        try {
            if($item = File::find($input->id)){
                $item->delete();
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Berhasil menghapus data'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }
}