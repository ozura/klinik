<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Account;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\PointsLog;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Setting;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use PDF;
use Validator;

class CartController extends BaseController{
    public function indexList(Request $request){
        $account = Auth::user();

        if($cart = Cart::with('customer')->where('seller_id', $account->account_id)->first()){
            // Next
        }else{
            DB::beginTransaction();

            try {
                $cart = new Cart;
                $cart->seller_id = $account->account_id;
                $cart->save();
                DB::commit();
                
            } catch (\Exception $e) {
                DB::rollback();

                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed'
                ]);
            }
        }

        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Penjualan', 'link' => 'cart']
        );

        return view('pages/cart/common-list', compact('breadcrumb', 'cart'));
    }
    /* API */
    public function commonList(Request $request){
        $account = Auth::user();
        if($cart = Cart::with('customer')->where('seller_id', $account->account_id)->first()){
            $list_data = CartDetail::with('product', 'product.image', 'product.purchase_prices')->where('cart_id', $cart->cart_id);
        }else{
            $list_data = null;
        }

        return Datatables::of($list_data)
                ->addColumn('selling_price', function($item) use ($cart){
                    if(!empty($cart->customer_id)){
                        return 'Rp'.number_format($item->product->purchase_prices->where('level_id', $cart->customer->level_id)->first()->price, 0, ',', '.');
                    }else{
                        return 'Rp'.number_format($item->product->purchase_prices->where('level_id', 5)->first()->price, 0, ',', '.');
                    }
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->cart_detail_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionCheckout(Request $request){
        $input = (object) $request->input();
        
        $validator = Validator::make($request->all(), [
            'key' => 'required'
        ]);
        
        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        if($input->key != 'PtwwBO0VwkWXa0C9RKtX') {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => 'Operation error'
            ]);
        }

        $account = Auth::user();

        if($cart = Cart::where(['seller_id' => $account->account_id])->first()){
            $cart_details = CartDetail::with('product', 'product.purchase_prices')->where('cart_id', $cart->cart_id)->get();
            $tanggal_transaksi = Carbon::now('Asia/Jakarta');
            $subtotal = 0;

            if(!$cart_details->first()){
                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed',
                    'message' => 'Keranjang kosong'
                ]);
            }

            foreach ($cart_details as $cart_detail) {
                if(!empty($cart->customer_id)){
                    $subtotal += $cart_detail->quantity * $cart_detail->product->purchase_prices->where('level_id', $cart->customer->level_id)->first()->price;
                }else{
                    $subtotal += $cart_detail->quantity * $cart_detail->product->purchase_prices->where('level_id', 5)->first()->price;
                }

                if($product_stock = ProductStock::where(['product_id' => $cart_detail->product_id, 'account_id' => $account->account_id])->first()){
                    $product_stock->final_stock = $product_stock->final_stock;
                    if($product_stock->final_stock < $cart_detail->quantity){
                        $cart_detail->delete();
                        return response()->json([
                            'status_code' 	=> 201,
                            'status_text' 	=> 'Failed',
                            'message' => 'Stok barang tidak mencukupi, barang tsb telah dikeluarkan dari keranjang'
                        ]);
                    }
                }else{
                    $cart_detail->delete();
                    return response()->json([
                        'status_code' 	=> 201,
                        'status_text' 	=> 'Failed',
                        'message' => 'Stok barang tidak mencukupi, barang tsb telah dikeluarkan dari keranjang'
                    ]);
                }
            }

            DB::beginTransaction();

            try {
                $transaction = new Transaction;
                $transaction->transaction_number = $this->generateNumberTransaction($tanggal_transaksi);
                $transaction->transaction_status = 1;
                $transaction->seller_id = $cart->seller_id;
                $transaction->customer_id = $cart->customer_id;
                $transaction->shipping_address_id = null;
                $transaction->shipping_service_id = null;
                $transaction->payment_method = null;
                $transaction->subtotal = $subtotal;
                $transaction->shipping_price = 0;
                $transaction->shipping_etl = 0;
                $transaction->save();

                foreach ($cart_details as $cart_detail) {
                    $transaction_detail                        = new TransactionDetail;
                    $transaction_detail->transaction_id = $transaction->transaction_id;
                    $transaction_detail->product_id = $cart_detail->product_id;
                    if(!empty($transaction->customer_id)){
                        $transaction_detail->price = $cart_detail->quantity * $cart_detail->product->purchase_prices->where('level_id', $cart->customer->level_id)->first()->price;
                    }else{
                        $transaction_detail->price = $cart_detail->quantity * $cart_detail->product->purchase_prices->where('level_id', 5)->first()->price;
                    }
                    $transaction_detail->quantity = $cart_detail->quantity;
                    $transaction_detail->save();

                    if($product_stock = ProductStock::where(['product_id' => $transaction_detail->product_id, 'account_id' => $account->account_id])->first()){
                        $product_stock->final_stock = $product_stock->final_stock - $transaction_detail->quantity;
                        $product_stock->sold_stock = $product_stock->sold_stock + $transaction_detail->quantity;
                        $product_stock->save();

                        if(!empty($transaction->customer_id)){
                            if($customer_product_stock = ProductStock::where(['product_id' => $transaction_detail->product_id, 'account_id' => $transaction->customer_id])->first()){
                                $customer_product_stock->final_stock = $customer_product_stock->final_stock + $transaction_detail->quantity;
                                $customer_product_stock->save();
                            }else{
                                $customer_product_stock = new ProductStock;
                                $customer_product_stock->account_id = $transaction->customer_id;
                                $customer_product_stock->product_id = $transaction_detail->product_id;
                                $customer_product_stock->final_stock = $transaction_detail->quantity;
                                $customer_product_stock->sold_stock = 0;
                                $customer_product_stock->save();
                            }
                        }
                    }
                }

                if(!empty($transaction->customer_id)){
                    $points = floor($transaction->subtotal / Setting::where('name', 'to_get_points')->first()->content) * Setting::where('name', 'points_get')->first()->content;
                    
                    if($points > 0){
                        $points_log = new PointsLog;
                        $points_log->transaction_id = $transaction->transaction_id;
                        $points_log->account_id = $transaction->customer_id;
                        $points_log->status = 'in';
                        $points_log->description = 'Anda mendapatkan '.$points.' poin dari transaksi #'.$transaction->transaction_number;
                        $points_log->points = $points;
                        $points_log->save();

                        $customer = Account::find($transaction->customer_id);
                        $customer->points = $customer->points + $points;
                        $customer->save();
                    }
                }

                CartDetail::where('cart_id', $cart->cart_id)->delete();
                Cart::where('seller_id', $account->account_id)->delete();

                DB::commit();

                return response()->json([
                    'status_code' => 200,
                    'status_text' => 'Success',
                    'message' => '',
                    'data' => array(
                        'transaction_number' => $transaction->transaction_number
                    )
                ]);
            } catch (\Exception $e) {
                DB::rollback();

                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed',
                    'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
                ]);
            }
        }else{
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => 'Operation error'
            ]);
        }
    }

    public function actionAddCart(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = Auth::user();

        if($cart = Cart::where('seller_id', $account->account_id)->first()) {
            if($product = Product::find($input->product_id)){
                if($product_stock = ProductStock::where(['product_id' => $input->product_id, 'account_id' => $account->account_id])->first()){
                    if($product_stock->final_stock < 1){
                        return response()->json([
                            'status_code' => 201,
                            'status_text' => 'Failed',
                            'message' => 'Stok tidak mencukupi untuk dijual'
                        ]);
                    }
                }else{
                    return response()->json([
                        'status_code' => 201,
                        'status_text' => 'Failed',
                        'message' => 'Barang tidak bisa ditambahkan ke keranjang'
                    ]);
                }

                if($cart_detail = CartDetail::where(['product_id' => $input->product_id, 'cart_id' => $cart->cart_id])->first()){
                    return response()->json([
                        'status_code' => 201,
                        'status_text' => 'Failed',
                        'message' => 'Sudah terdapat barang yang sama dalam keranjang'
                    ]);
                }
            }
        }

        DB::beginTransaction();

        try {
            $cart_detail = new CartDetail;
            $cart_detail->cart_id = $cart->cart_id;
            $cart_detail->product_id = $input->product_id;
            $cart_detail->quantity = 1;
            $cart_detail->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Barang sudah ditambahkan ke keranjang'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionAddQuantiyCart(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'cart_detail_id' => 'required|integer',
            'quantity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = Auth::user();

        DB::beginTransaction();

        try {
            if($cart_detail = CartDetail::find($input->cart_detail_id)){
                if($product_stock = ProductStock::where(['product_id' => $cart_detail->product_id, 'account_id' => $account->account_id])->first()){
                    $new_quantity = $cart_detail->quantity + $input->quantity;
                    if($new_quantity < 1){
                        return response()->json([
                            'status_code' 	=> 201,
                            'status_text' 	=> 'Failed',
                            'message' => 'Terjadi kesalahan'
                        ]);
                    }else if($new_quantity > $product_stock->final_stock){
                        return response()->json([
                            'status_code' 	=> 201,
                            'status_text' 	=> 'Failed',
                            'message' => 'Stok tidak mencukupi untuk dijual'
                        ]);
                    }else{
                        $cart_detail->quantity = $new_quantity;
                        $cart_detail->save();
                    }
                }
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'OK'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'cart_detail_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        DB::beginTransaction();

        try {
            if($cart_detail = CartDetail::find($input->cart_detail_id)){
                $cart_detail->delete();
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Berhasil menghapus Barang di keranjang'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionCustomerAdd(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = Auth::user();

        DB::beginTransaction();

        try {
            $cart = Cart::where('seller_id', $account->account_id)->first();
            $cart->customer_id = $input->customer_id;
            $cart->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Berhasil memilih customer'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionCustomerDelete(Request $request){
        $input = (object) $request->input();

        $account = Auth::user();

        DB::beginTransaction();

        try {
            $cart = Cart::where('seller_id', $account->account_id)->first();
            $cart->customer_id = null;
            $cart->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Berhasil menghapus customer dari cart'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function generateNumberTransaction($tanggal_transaksi){
        $tanggal_totime = strtotime($tanggal_transaksi);

        $tahun = date("Y", $tanggal_totime);
        $bulan = date("m", $tanggal_totime);
        $tanggal = date("d", $tanggal_totime);

        $max_nomor_transaksi = DB::select('SELECT MAX(transaction_number) AS maxID FROM `transactions`')[0]->maxID; 
        if ($max_nomor_transaksi == '') { 
            $nomor_transaksi = "TR" . $tahun . "" . $bulan . "" . $tanggal . "000001"; 
        } else { 
            $max_id = substr($max_nomor_transaksi, 2, 14); 
            $nomor_urut = (int) substr($max_id, 8, 6); 
            $nomor_bulan = (int) substr($max_id, 4, 2); 
            if ($nomor_bulan != $bulan) { 
                $nomor_urut = 1; 
            } else {
                $nomor_urut++; 
            } 
            $nomor_transaksi = "TR" . $tahun . "" . $bulan . "" . $tanggal . sprintf("%06s", $nomor_urut); 
        }
        return $nomor_transaksi;
    }

}