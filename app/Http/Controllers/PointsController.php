<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

use App\Models\AccountReward;
use App\Models\PointsLog;

use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class PointsController extends BaseController{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Poin Saya', 'link' => 'points']
        );

        $account = Auth::user();

        return view('pages/points/common-list', compact('breadcrumb', 'account'));
    }

    public function indexDetail(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Poin Saya', 'link' => 'points'],
            (object) ['name' => 'Detail', 'link' => 'points/detail']
        );

        $account = Auth::user();

        return view('pages/points/detail-list', compact('breadcrumb', 'account'));
    }

    public function indexExchanged(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Poin Saya', 'link' => 'points'],
            (object) ['name' => 'Reward Saya', 'link' => 'reward/exchange']
        );

        $account = Auth::user();

        return view('pages/points/exchanged-list', compact('breadcrumb', 'account'));
    }

    /* API */
    public function commonList(Request $request){
        $account = Auth::user();
        $list_data = PointsLog::where('account_id', $account->account_id);

        return Datatables::of($list_data)
                ->make(true);
    }

    public function exchangedList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $list_data = AccountReward::with('reward', 'reward.image')->where('account_id', $account->account_id)->orderBy('created_at', 'desc');

        return Datatables::of($list_data)
                ->addColumn('status', function($item){
                    return $item->status_to_text();
                })
                ->editColumn('created_at', function($item){
                    return date_format(date_create($item->created_at),"d M Y H:i").' WIB';
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->reward_id
                    );
                    return $data;
                })
                ->make(true);
    }
}