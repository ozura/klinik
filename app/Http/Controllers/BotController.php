<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Models\BARANG as Barang;
use App\Models\STOK as Stok;
use App\Models\TRANSAKSI as Transaksi;
use App\Models\TRANSAKSIDETAIL as Transaksidetail;

class BotController extends BaseController{
    public function fetchTransaction(Request $request){
        return Transaksi::count();
    }

    public function pullTransaction(Request $request){
        $input = (object) $request->input();
        if(isset($input->count)){
            $status = 200;
            $transaksi = Transaksi::where('ID_TRANSAKSI', '>', $input->count)->get();
            $transaksi_detail = Transaksidetail::where('ID_TRANSAKSI', '>', $input->count)->get();

            if($transaksi->first()){
                return compact('status', 'transaksi', 'transaksi_detail');
            }else{
                return ['status' => 201, 'message' => 'Nothing data to sync!'];
            }
        }else{
            return ['status' => 201, 'message' => 'Sync failed!'];
        }
    }

    public function pushTransaction(Request $request){
        $input = (object) $request->input();

        $data_transaksi = collect($input->transaksi);
        foreach($data_transaksi->chunk(35) as $chunk){
            $data = array();
            foreach($chunk as $temp_transaksi){
                $temp_transaksi = (object) $temp_transaksi;
                if($item = Transaksi::find($temp_transaksi->ID_TRANSAKSI)){
                }else{
                    $transaksi = array(
                        'ID_TRANSAKSI' => $temp_transaksi->ID_TRANSAKSI,
                        'ID_PEGAWAI' => $temp_transaksi->ID_PEGAWAI,
                        'NO_TRANSAKSI' => $temp_transaksi->NO_TRANSAKSI,
                        'TANGGAL_TRANSAKSI' => $temp_transaksi->TANGGAL_TRANSAKSI,
                        'SUBTOTAL_TRANSAKSI' => $temp_transaksi->SUBTOTAL_TRANSAKSI,
                        'POTONGAN' => $temp_transaksi->POTONGAN,
                        'PROFIT_BERSIH_TRANSAKSI' => $temp_transaksi->PROFIT_BERSIH_TRANSAKSI,
                        'ZAKAT_TRANSAKSI' => $temp_transaksi->ZAKAT_TRANSAKSI,
                        'INFAQ_TRANSAKSI' => $temp_transaksi->INFAQ_TRANSAKSI,
                        'PROFIT_AKHIR' => $temp_transaksi->PROFIT_AKHIR
                    );

                    $data[] = $transaksi;
                }
            }

            Transaksi::insert($data);
        }

        $data_transaksi_detail = collect($input->transaksi_detail);
        foreach($data_transaksi_detail->chunk(35) as $chunk){
            $data = array();
            foreach($chunk as $temp_transaksi_detail){
                $temp_transaksi_detail = (object) $temp_transaksi_detail;
                if($item = Transaksidetail::find($temp_transaksi_detail->ID_TRANSAKSI_DETAIL)){
                }else{
                    $transaksi_detail = array(
                        'ID_TRANSAKSI_DETAIL' => $temp_transaksi_detail->ID_TRANSAKSI_DETAIL,
                        'ID_TRANSAKSI' => $temp_transaksi_detail->ID_TRANSAKSI,
                        'ID_BARANG' => $temp_transaksi_detail->ID_BARANG,
                        'ID_SIZE_DETAIL' => $temp_transaksi_detail->ID_SIZE_DETAIL,
                        'QTY' => $temp_transaksi_detail->QTY,
                        'HARGA_MASUK' => $temp_transaksi_detail->HARGA_MASUK,
                        'HARGA_AKHIR' => $temp_transaksi_detail->HARGA_AKHIR,
                        'IS_DISKON' => $temp_transaksi_detail->IS_DISKON,
                        'IS_BUY1_GET1' => $temp_transaksi_detail->IS_BUY1_GET1,
                        'PPN' => $temp_transaksi_detail->PPN,
                        'PROFIT_KOTOR' => $temp_transaksi_detail->PROFIT_KOTOR,
                        'PROFIT_BERSIH' => $temp_transaksi_detail->PROFIT_BERSIH,
                        'PROFIT_SUPPLIER' => $temp_transaksi_detail->PROFIT_SUPPLIER
                    );

                    $data[] = $transaksi_detail;

                    if($stok = Stok::where(['ID_BARANG' => $temp_transaksi_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $temp_transaksi_detail->ID_SIZE_DETAIL])->first()){
                        $stok->STOK_JUAL = $stok->STOK_JUAL + $temp_transaksi_detail->QTY;
                        $stok->STOK_AKHIR = $stok->STOK_AKHIR - $temp_transaksi_detail->QTY;
                        $stok->save();

                        $barang = Barang::find($temp_transaksi_detail->ID_BARANG);
                        $barang->STOK_GLOBAL = $barang->STOK_GLOBAL - $temp_transaksi_detail->QTY;
                        $barang->save();
                    }
                }
            }

            Transaksidetail::insert($data);
        }
        return ['status' => 200, 'message' => 'Successfully sync transaction!'];
    }
}