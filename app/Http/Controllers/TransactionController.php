<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use PDF;
use Session;

class TransactionController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Laporan Penjualan', 'link' => 'transaction']
        );

        return view('pages/transaction/common-list', compact('breadcrumb'));
    }

    public function indexNotaPrint(Request $request){
        $input = (object) $request->input();

        if($transaction = Transaction::with('seller', 'customer')->where('transaction_number', $input->transaction_number)->first()){
            $transaction_details = TransactionDetail::with('product')->where('transaction_id', $transaction->transaction_id)->get();

            $pdf = PDF::loadView('print/nota', compact('transaction', 'transaction_details'))->setPaper('a8', 'portrait');
            return $pdf->stream('nota-'.$transaction->transaction_number.'.pdf');
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    /* API */
    public function commonList(Request $request){
        $input = (object) $request->input();
        $account = Auth::user();
        $list_data = Transaction::with('customer')->where('seller_id', $account->account_id)->orderBy('created_at', 'desc');

        if(!empty($input->startdate)){
            $start_date = Carbon::parse(urldecode($input->startdate));
            $list_data = $list_data->where('created_at' , '>=', $start_date);
        }

        if(!empty($input->enddate)){
            $end_date = Carbon::parse(urldecode($input->enddate));
            $list_data = $list_data->where('created_at' , '<=', $end_date);
        }

        return Datatables::of($list_data)
                ->addColumn('customer_name', function($item){
                    if(!empty($item->customer_id)){
                        return $item->customer->name;
                    }else{
                        return 'Guest';
                    }
                })
                ->addColumn('transaction_date', function($item){
                    return date_format(date_create($item->created_at),"d M Y H:i").' WIB';
                })
                ->editColumn('subtotal', function($item){
                    if($item->subtotal > 0){
                        $status = 'normal';
                    }else{
                        $status = 'cancel';
                    }
                    $data = array(
                        'status' => $status,
                        'subtotal' => 'Rp'.number_format($item->subtotal, 0, ',', '.')
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    if($item->subtotal > 0){
                        $status = 'normal';
                    }else{
                        $status = 'cancel';
                    }
                    $data = array(
                        'id' => $item->transaction_id,
                        'number' => $item->transaction_number,
                        'status' => $status
                    );
                    return $data;
                })
                ->make(true);
    }

    public function detailList(Request $request){
        $input = (object) $request->input();

        if(empty($input->transaction_id)){
            $list_data = array();
        }else{
            $list_data = TransactionDetail::with('product')->where('transaction_id', $input->transaction_id);
        }

        return Datatables::of($list_data)
                ->editColumn('price', function($item){
                    return 'Rp'.number_format($item->price, 0, ',', '.');
                })
                ->make(true);
    }

    public function chartList(Request $request){
        $input = (object) $request->input();
        $account = Auth::user();
        $list_data = Transaction::select('subtotal', 'created_at')->where('seller_id', $account->account_id)->orderBy('created_at', 'asc');

        if(!empty($input->startdate)){
            $start_date = Carbon::parse(urldecode($input->startdate));
            $list_data = $list_data->where('created_at' , '>=', $start_date);
        }

        if(!empty($input->enddate)){
            $end_date = Carbon::parse(urldecode($input->enddate));
            $list_data = $list_data->where('created_at' , '<=', $end_date);
        }

        $list_data = $list_data->get()->groupBy(function($item) {
            return Carbon::parse($item->created_at)->format('d M Y'); // grouping by months
        })->map(function ($row) {
            return $row->sum('subtotal');
        });

        return $list_data;
    }

    public function productChartList(Request $request){
        $input = (object) $request->input();
        $account = Auth::user();
        $list_data = TransactionDetail::with('product')->orderBy('created_at', 'asc');

        $list_data = $list_data->whereHas('transaction', function($q) use ($account, $input){
            $q->where('seller_id', $account->account_id);
            
            if(!empty($input->startdate)){
                $start_date = Carbon::parse(urldecode($input->startdate));
                $q->where('created_at' , '>=', $start_date);
            }

            if(!empty($input->enddate)){
                $end_date = Carbon::parse(urldecode($input->enddate));
                $q->where('created_at' , '<=', $end_date);
            }
        });

        $list_data = $list_data->get()->groupBy(function($item) {
            return $item->product->name;
        })->map(function ($row) {
            return $row->sum('quantity');
        });

        return $list_data;
    }

    public function actionRetur(Request $request){
        $input = (object) $request->input();
        $pengguna = Auth::user();
        if($pengguna->USERNAME != 'admin'){
            return abort(404);
        }

        if($nota_transaksi = Notatransaksi::where(['NO_TRANSAKSI' => $input->NO_TRANSAKSI, 'METODE_PEMBAYARAN' => 'Offline Store'])->first()){
            foreach(Notatransaksidetail::where('ID_NOTA_TRANSAKSI', $nota_transaksi->ID_NOTA_TRANSAKSI)->get() as $nota_transaksi_detail){
                if($stok = Stok::where(['ID_BARANG' => $nota_transaksi_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $nota_transaksi_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 2])->first()){
                    $barang = Barang::find($nota_transaksi_detail->ID_BARANG);
                    $barang->STOK_GLOBAL = $barang->STOK_GLOBAL + $nota_transaksi_detail->QTY;
                    $barang->save();
                    
                    $stok->STOK_JUAL = $stok->STOK_JUAL - $nota_transaksi_detail->QTY;
                    $stok->STOK_AKHIR = $stok->STOK_AKHIR + $nota_transaksi_detail->QTY;
                    $stok->save();
                    
                    $nota_transaksi_detail->QTY = 0;
                    $nota_transaksi_detail->save();
                }
            }
    
            $nota_transaksi->STATUS_TRANSAKSI = 7;
            $nota_transaksi->TEMP_STATUS_TRANSAKSI = 22;
            $nota_transaksi->TRANSACTION_STATUS = 'expire';
            $nota_transaksi->save();
        }else{
            return abort(404);
        }

        if($transaksi = Transaksi::where('NO_TRANSAKSI', $nota_transaksi->NO_TRANSAKSI)->first()){
            $today = Carbon::now();
            foreach(Transaksidetail::where('ID_TRANSAKSI', $transaksi->ID_TRANSAKSI)->get() as $transaksi_detail){
                $transaksi_refund = new Transaksirefund;
                $transaksi_refund->ID_TRANSAKSI_DETAIL = $transaksi_detail->ID_TRANSAKSI_DETAIL;
                $transaksi_refund->QTY = $transaksi_detail->QTY;
                $transaksi_refund->STATUS_REFUND = 2;
                $transaksi_refund->STATUS_TERIMA_DARI_CUSTOMER = 3;
                $transaksi_refund->KETERANGAN = 'Refund offline store';
                $transaksi_refund->KONDISI = 1;
                $transaksi_refund->TANGGAL_REQUEST = $today;
                $transaksi_refund->TANGGAL_EXPIRED_REQUEST = $today;
                $transaksi_refund->TANGGAL_TERIMA_BARANG = $today;
                $transaksi_refund->TANGGAL_TRANSFER = $today;
                $transaksi_refund->NOMOR_REKENING = '0000';
                $transaksi_refund->NAMA_PEMILIK_REKENING = '0000';
                $transaksi_refund->ID_BANK = 22;
                $transaksi_refund->ID_ALASAN_RETUR = 0;
                $transaksi_refund->ID_PEGAWAI_ACTION = $pengguna->ID_PENGGUNA;
                $transaksi_refund->JUMLAH_NOMINAL_TRANSFER = $transaksi_detail->QTY * $transaksi_detail->HARGA_AKHIR;
                $transaksi_refund->ID_PEGAWAI_TRANSFER = $pengguna->ID_PENGGUNA;
                $transaksi_refund->save();

                $transaksi_detail->QTY              = 0;
                $transaksi_detail->PROFIT_KOTOR     = 0;
                $transaksi_detail->PROFIT_BERSIH    = 0;
                $transaksi_detail->PROFIT_SUPPLIER  = 0;
                $transaksi_detail->save();
            }

            $transaksi->PROFIT_BERSIH_TRANSAKSI = 0;
            $transaksi->ZAKAT_TRANSAKSI         = 0;
            $transaksi->INFAQ_TRANSAKSI         = 0;
            $transaksi->PROFIT_LOKALGOODS       = 0;
            $transaksi->save();
        }
        
        return ['status' => 200, 'message' => 'Retur record!'];
    }

}