<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Libraries\HelperFunction;

use App\Models\AccountReward;
use App\Models\Image;
use App\Models\Reward;
use Yajra\Datatables\Datatables;

use Carbon\Carbon;

use Auth;
use DB;
use Session;
use Validator;

class RewardController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Reward', 'link' => 'reward']
        );

        return view('pages/reward/common-list', compact('breadcrumb'));
    }

    public function indexManageItem(Request $request, $id = 0){
        if($item = Reward::find($id)){
            $breadcrumb_item = (object) array(
                'name' => 'Edit Reward', 
                'link' => 'reward/edit/' .$id
            );
        }else{
            $item = null;
            $breadcrumb_item = (object) array(
                'name' => 'Tambah Reward', 
                'link' => 'reward/add'
            );
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Reward', 'link' => 'reward'],
            $breadcrumb_item
        );

        return view('pages/reward/manage-item', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $today = Carbon::today();
        $list_data = Reward::with('image')->orderBy('name');

        if($account->level_id != 1){
            $list_data  = $list_data->where('start_date', '<=', $today)->where('end_date', '>=', $today);
        }
        
        return Datatables::of($list_data)
                ->editColumn('change_points', function($item){
                    return $item->change_points.' poin';
                })
                ->addColumn('status', function($item) use ($today){
                    if($item->start_date <= $today && $today <= $item->end_date){
                        return 'Aktif';
                    }else{
                        return 'Tidak Aktif';
                    }
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->reward_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionExchanged(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = $input->account;
        $today = Carbon::today();
        
        if($account->level_id == 1) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => 'Admin tidak diperbolehkan menukarkan reward'
            ]);
        }

        if($reward = Reward::find($input->id)){
            if($reward->start_date > $today || $today > $reward->end_date){
                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed',
                    'message' => 'Masa reward sudah tidak berlaku'
                ]);
            }

            if($reward->available_stock <= 0){
                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed',
                    'message' => 'Reward sudah habis ditukarkan'
                ]);
            }

            if($account->points < $reward->change_points){
                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed',
                    'message' => 'Poin Anda tidak mencukupi untuk penukaran reward'
                ]);
            }
        }else{
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => 'Reward tidak ditemukan'
            ]);
        }

        DB::beginTransaction();

        try {
            $loaduri = 'points';

            $item = new AccountReward;
            $item->account_id = $account->account_id;
            $item->reward_id = $reward->reward_id;
            $item->status = 'preprocessing';
            $item->save();

            $account->points = $account->points - $reward->change_points;
            $account->save();

            $reward->available_stock = $reward->available_stock - 1;
            $reward->exchanged_stock = $reward->exchanged_stock + 1;
            $reward->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Penukaran berhasil',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'change_points' => 'required',
            'available_stock' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = Auth::user();
        
        DB::beginTransaction();

        try {
            if(empty($input->id)){
                $item = new Reward;
                $loaduri = 'reward';
            }else{
                if($item = Reward::find($input->id)){
                    // Next
                    $loaduri = 'reward/edit/'.$input->id;
                }
            }

            if(!empty($request->input('image'))){
                $file = $request->input('image');
                $tanggal = Carbon::today()->format('Y-m-d');

                $image_helper = HelperFunction::uploadBase64File($file, $tanggal);
    
                $image = new Image;
                $image->name = 'Cover reward '.$input->name;
                $image->type = $image_helper->ext;
                $image->image_url = $image_helper->path;
                $image->save();

                $item->image_id = $image->image_id;
            }
    
            $item->name = $input->name;
            $item->description = $input->description;
            $item->change_points = $input->change_points;
            $item->available_stock = $input->available_stock;
            $item->start_date = $input->start_date;
            $item->end_date = $input->end_date;
            $item->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        DB::beginTransaction();

        try {
            if($item = Reward::find($input->id)){
                $item->delete();
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Berhasil menghapus data'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }
}