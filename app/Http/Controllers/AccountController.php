<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use App\Libraries\HelperFunction;

use App\Models\Account;
use App\Models\Address;
use App\Models\Cart;
use App\Models\City;
use App\Models\District;
use App\Models\File;
use App\Models\Image;
use App\Models\Level;
use App\Models\Post;
use App\Models\Province;
use App\Models\Religion;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class AccountController extends BaseController{
    public function indexSignIn(Request $request){
        if(Auth::check()){
            return redirect('dashboard');
        }else{
            return view('sign-in');
        }
    }

    public function indexDashboard(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        return view('dashboard', compact('account'));
    }

    public function indexWelcome(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome']
        );

        $files = File::orderBy('name')->limit(5)->get();
        $posts = Post::with('image')->orderBy('title')->limit(5)->get();

        return view('pages/welcome', compact('breadcrumb', 'account', 'posts', 'files'));
    }

    public function indexList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Akun', 'link' => 'account']
        );

        return view('pages/account/common-list', compact('breadcrumb', 'account'));
    }

    public function indexManageItem(Request $request, $id = 0){
        $input = (object) $request->input();
        $account = $input->account;

        
        if($account->level_id == 1){
            if($item = Account::with('ktp', 'stay', 'photo', 'identification', 'aggrement')->where('account_id', $id)->first()){
                $breadcrumb_item = (object) array(
                    'name' => 'Edit Akun', 
                    'link' => 'account/edit/' .$id
                );
            }else{
                $item = null;
                $breadcrumb_item = (object) array(
                    'name' => 'Tambah Akun', 
                    'link' => 'account/add'
                );
            }

            $levels = Level::where('level_id', '<>', 1)->orderBy('level_id', 'asc')->get();

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Data Akun', 'link' => 'account'],
                $breadcrumb_item
            );
    
            $religions = Religion::get();
            $provinces = Province::orderBy('name')->get();
            $cities = City::orderBy('name')->get();
            $districts = District::orderBy('name')->get();

            return view('pages/account/manage-item', compact('breadcrumb', 'item', 'levels', 'religions', 'provinces', 'cities', 'districts'));
        }else{
            if($item = Account::where('account_id', $id)->first()){
                $breadcrumb_item = (object) array(
                    'name' => 'Edit Akun', 
                    'link' => 'account/simple-edit/' .$id
                );
            }else{
                $item = null;
                $breadcrumb_item = (object) array(
                    'name' => 'Tambah Akun', 
                    'link' => 'account/simple-add'
                );
            }
            
            $levels = Level::where('level_id', 5)->orderBy('level_id', 'asc')->get();

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Data Akun', 'link' => 'account'],
                $breadcrumb_item
            );
    
            return view('pages/account/manage-item-simple', compact('breadcrumb', 'item', 'levels'));
        }
    }

    /* API */
    public function commonList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $list_data = Account::with('level', 'photo')->where('account_id', '<>', $account->account_id)->where('level_id', '<>', 1);

        return Datatables::of($list_data)
                ->addColumn('status', function($item){
                    return $item->level->name;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->account_id,
                        'name' => $item->name,
                        'level' => $item->level_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function customerList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $list_data = Account::with('level')->where('level_id', '>', $account->level_id);

        return Datatables::of($list_data)
                ->addColumn('status', function($item){
                    return $item->level->name;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->account_id,
                        'name' => $item->name
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'level_id' => 'required',
            'religion_id' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'married_status' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = $input->account;
        
        DB::beginTransaction();

        try {
            if(empty($input->id)){
                $item = new Account;
                $loaduri = 'account';
                
                $address_ktp = new Address;
                $address_ktp->type = 'ktp';
                $address_stay = new Address;
                $address_stay->type = 'stay';
                $item->password = Hash::make('123456');
            }else{
                if($item = Account::find($input->id)){
                    // Next
                    $loaduri = 'account/edit/'.$input->id;

                    $address_ktp = $item->ktp;
                    $address_stay = $item->stay;
                }
            }

            if(!empty($request->input('photo'))){
                $file = $request->input('photo');
                $tanggal = Carbon::today()->format('Y-m-d');

                $image_helper = HelperFunction::uploadBase64File($file, $tanggal);
    
                $image = new Image;
                $image->name = 'Foto 3x4 '.$input->name;
                $image->type = $image_helper->ext;
                $image->image_url = $image_helper->path;
                $image->save();

                $item->image_id = $image->image_id;
            }
            
            if(!empty($request->input('identification'))){
                $file = $request->input('identification');
                $tanggal = Carbon::today()->format('Y-m-d');

                $image_helper = HelperFunction::uploadBase64File($file, $tanggal);
    
                $image = new Image;
                $image->name = 'Foto KTP '.$input->name;
                $image->type = $image_helper->ext;
                $image->image_url = $image_helper->path;
                $image->save();

                $item->image_identification = $image->image_id;
            }

            if(!empty($request->input('aggrement'))){
                $file = $request->input('aggrement');
                $tanggal = Carbon::today()->format('Y-m-d');

                $image_helper = HelperFunction::uploadBase64File($file, $tanggal);
    
                $image = new Image;
                $image->name = 'Foto MOU '.$input->name;
                $image->type = $image_helper->ext;
                $image->image_url = $image_helper->path;
                $image->save();

                $item->image_aggrement = $image->image_id;
            }

            $address_ktp->address = $input->address_ktp;
            $address_ktp->province_id = $input->province_ktp;
            $address_ktp->city_id = $input->city_ktp;
            $address_ktp->district_id = $input->district_ktp;
            $address_ktp->postal_code = $input->postal_code_ktp;
            $address_ktp->save();

            $address_stay->address = $input->address_stay;
            $address_stay->province_id = $input->province_stay;
            $address_stay->city_id = $input->city_stay;
            $address_stay->district_id = $input->district_stay;
            $address_stay->postal_code = $input->postal_code_stay;
            $address_stay->save();
    
            $item->email = $input->email;
            $item->name = $input->name;
            $item->phone = $input->phone;
            $item->username = $input->email;
            $item->level_id = $input->level_id;
            $item->religion_id = $input->religion_id;
            $item->gender = $input->gender;
            $item->birthday = $input->birthday;
            $item->married_status = $input->married_status;
            $item->address_ktp = $address_ktp->address_id;
            $item->address_stay = $address_stay->address_id;
            $item->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionSimpleSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'gender' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = $input->account;
        
        DB::beginTransaction();

        try {
            if(empty($input->id)){
                $item = new Account;
                $loaduri = 'account';
                
                $item->password = Hash::make('123456');
                $address_ktp = new Address;
                $address_ktp->type = 'ktp';
                $address_ktp->save();
                
                $address_stay = new Address;
                $address_stay->type = 'stay';
                $address_stay->save();

                $item->address_ktp = $address_ktp->address_id;
                $item->address_stay = $address_stay->address_id;
            }else{
                if($item = Account::find($input->id)){
                    // Next
                    $loaduri = 'account/simple-edit/'.$input->id;
                }
            }

            $item->name = $input->name;
            if(!empty($input->email)){
                $item->email = $input->email;
                $item->username = $input->email;
            }else{
                $item->phone = $input->phone;
                $item->username = $input->phone;
            }
            $item->level_id = 5;
            $item->gender = $input->gender;
            
            $item->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionSignIn(Request $request){
        $input = (object) $request->input();

        $account = Account::where(['email' => $input->username])->first();
        
        if ($account && Hash::check($input->password, $account->password)) {
            Auth::login($account);
            return redirect('dashboard');
        }else{
            Session::flash('toast-error', 'Sign in failed');
            return redirect()->back()->withInput();
        }
    }

    public function actionSignOut(Request $request){
        $input = (object) $request->input();
        Auth::logout();
        Session::flash('toast-success', 'Sign out successfully');
        return redirect('/');
    }

    public function actionChangePassword(Request $request){
        $input = (object) $request->input();
        $account = $input->account;
        
        if($input->new_password == $input->new_confirm_password){
            if ($account && Hash::check($input->old_password, $account->password)) {
                $account->password = Hash::make($input->new_password);
                $account->save();
                return ['status' => 200, 'message' => 'Sukses mengganti password'];
            }else{
                return ['status' => 201, 'message' => 'Your old password is incorrect'];
            }
        }else{
            return ['status' => 201, 'message' => 'Re-type your new password again'];
        }
    }

}