<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Models\CUSTOMER as Customer;
use App\Models\CUSTOMERDATANG as Customerdatang;
use App\Models\PENGGUNA as Pengguna;
use Yajra\Datatables\Datatables;

use Carbon\Carbon;

use Auth;
use DB;
use Session;
use Validator;

class CustomerController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Customer Datang', 'link' => 'customer-datang']
        );

        return view('pages/list-customer-datang', compact('breadcrumb'));
    }

    /* API */
    public function customerDatangList(Request $request){
        $input = (object) $request->input();

        $start_time = $input->start_time;
        $end_time = $input->end_time;

        $list_data = CustomerDatang::selectRaw('ID_GENDER, COUNT(*) AS JUMLAH_CUSTOMER')->whereBetween('WAKTU_DATANG', [$start_time, $end_time])
                            ->groupBy('ID_GENDER');
        
        return Datatables::of($list_data)
                ->addColumn('NM_GENDER', function($item){
                    if($item->ID_GENDER == 2){
                        return 'Perempuan';
                    }else{
                        return 'Laki-laki';
                    }
                })
                ->make(true);
    }

    public function commonList(Request $request){
        $list_data = Pengguna::has('customer')
                            ->where('STATUS_JOIN_PENGGUNA', 3)
                            ->orderBy('ID_PENGGUNA', 'desc');
        
        return Datatables::of($list_data)
                ->editColumn('status', function($item){
                    if($item->customer->KATEGORI_CUSTOMER == 2){
                        return 'member';
                    }else{
                        return 'customer';
                    }
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->ID_PENGGUNA,
                        'name' => $item->NM_PENGGUNA
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $validator = Validator::make($request->all(), [
            'NM_PENGGUNA' => 'required|min:3',
            'TELP_PENGGUNA' => 'required|min:8',
            'EMAIL_PENGGUNA' => 'nullable|email'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }
        
        $input = (object) $request->input();
        
        if($pengguna = Pengguna::where('TELP_PENGGUNA', $input->TELP_PENGGUNA)->first()){
            return ['status' => 201, 'message' => 'Nomor telepon telah digunakan'];
        }else if(!empty($input->EMAIL_PENGGUNA) && $pengguna = Pengguna::where('EMAIL_PENGGUNA', $input->EMAIL_PENGGUNA)->first()){
            return ['status' => 201, 'message' => 'Email telah digunakan'];
        }
        $pengguna                       = new Pengguna;
        $pengguna->NM_PENGGUNA          = $input->NM_PENGGUNA;
        $pengguna->ID_JABATAN           = 3;
        $pengguna->ID_GENDER            = 0;
        $pengguna->JOIN_DATE            = date("Y-m-d");
        $pengguna->STATUS_JOIN_PENGGUNA = 3;
        $pengguna->STATUS_PENGGUNA      = 1;
        $pengguna->TELP_PENGGUNA        = $input->TELP_PENGGUNA;
        if(!empty($input->EMAIL_PENGGUNA)){
            $pengguna->EMAIL_PENGGUNA   = $input->EMAIL_PENGGUNA;
        }
        $pengguna->save();

        $customer                           = new Customer;
        $customer->ID_PENGGUNA              = $pengguna->ID_PENGGUNA;
        $customer->ID_CABANG                = 1;
        $customer->KATEGORI_CUSTOMER        = 1;
        $customer->ID_SETTING_POIN          = 1;
        $customer->JOIN_DATE                = date("Y-m-d");
        $customer->ID_PEGAWAI               = Auth::user()->ID_PENGGUNA;
        $customer->STATUS_CUSTOMER          = 1;
        $customer->POIN                     = 0;
        $customer->KODE_VERIFIKASI          = rand(100000 , 999990);
        $customer->KODE_VERIFIKASI_EXPIRED  = Carbon::now(env('APP_TIMEZONE', ''))->addMinutes(30);
        $customer->save();

        return ['status' => 200, 'message' => 'Successfully save record!'];
    }

    public function actionDatangAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_GENDER' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }
        
        $input = (object) $request->input();
        
        $customer_datang = new Customerdatang;
        $customer_datang->ID_GENDER = $input->ID_GENDER;
        $customer_datang->ID_GUDANG = 2;
        $customer_datang->WAKTU_DATANG = Carbon::now();
        $customer_datang->save();

        $today = Carbon::today();
        $end_today = Carbon::today()->endOfDay();
        $data_customer_datang = Customerdatang::whereBetween('WAKTU_DATANG', [$today, $end_today])->where('ID_GUDANG', 2)->where('ID_GENDER', $input->ID_GENDER)->count();

        return ['status' => 200, 'message' => 'Successfully save record!', 'gender' => $data_customer_datang];
    }
}