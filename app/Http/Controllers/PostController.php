<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

use App\Libraries\HelperFunction;

use App\Models\Post;
use App\Models\PostCategory;
use App\Models\Image;

use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class PostController extends BaseController{
    
    public function indexList(Request $request){
        $input = (object) $request->input();
        $account = $input->account;

        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Tulisan', 'link' => 'post']
        );

        return view('pages/post/common-list', compact('breadcrumb', 'account'));
    }

    public function indexManageItem(Request $request, $id = 0){
        if($item = Post::find($id)){
            $breadcrumb_item = (object) array(
                'name' => 'Edit Tulisan', 
                'link' => 'post/edit/' .$id
            );
        }else{
            $item = null;
            $breadcrumb_item = (object) array(
                'name' => 'Tambah Tulisan', 
                'link' => 'post/add'
            );
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Tulisan', 'link' => 'post'],
            $breadcrumb_item
        );

        $post_categories = PostCategory::get();

        return view('pages/post/manage-item', compact('breadcrumb', 'item', 'post_categories'));
    }

    public function indexDetail(Request $request, $id = 0){
        if($item = Post::with('image', 'category')->where('post_id', $id)->first()){
            $breadcrumb_item = (object) array(
                'name' => 'Detail Tulisan', 
                'link' => 'post/detail/' .$id
            );
        }else{
            return abort(404);
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Data Tulisan', 'link' => 'post'],
            $breadcrumb_item
        );

        return view('pages/post/detail-item', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        $account = Auth::user();
        $list_data = Post::with('category', 'image');

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->post_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionUploadImage(Request $request){
        $input = (object) $request->input();

        if ($request->hasFile('image_base')) {
            $tanggal = Carbon::today()->format('Y-m-d');
            $filename = uniqid().'.'.$request->file('image_base')->getClientOriginalExtension();
            $image_helper = HelperFunction::uploadImage($request->file('image_base'), $tanggal);

            $image = new Image;
            $image->name = $request->file('image_base')->getClientOriginalName();
            $image->type = $image_helper->ext;
            $image->image_url = $image_helper->path;
            $image->save();

            return url($image->image_url);
        }else{
            return null;
        }
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'post_category_id' => 'required',
            'title' => 'required',
            'content' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        $account = Auth::user();
        
        DB::beginTransaction();

        try {
            if(empty($input->id)){
                $item = new Post;
                $loaduri = 'post';
            }else{
                if($item = Post::find($input->id)){
                    // Next
                    $loaduri = 'post/edit/'.$input->id;
                }
            }
            
            if(!empty($request->input('image'))){
                $file = $request->input('image');
                $tanggal = Carbon::today()->format('Y-m-d');

                $image_helper = HelperFunction::uploadBase64File($file, $tanggal);
    
                $image = new Image;
                $image->name = 'Cover tulisan '.$input->title;
                $image->type = $image_helper->ext;
                $image->image_url = $image_helper->path;
                $image->save();

                $item->image_id = $image->image_id;
            }

            $bom = '\xEF\xBB\xBF';

            $item->title = $input->title;
            $item->post_category_id = $input->post_category_id;
            $item->content = $input->content;
            $item->plain_text = str_replace($bom, '', ucfirst(strtolower(stripslashes(strip_tags($input->content)))));
            $item->save();

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Data berhasil disimpan',
                'loaduri' => $loaduri
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        DB::beginTransaction();

        try {
            if($item = Post::find($input->id)){
                $item->delete();
            }

            DB::commit();

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => 'Berhasil menghapus data'
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }
}