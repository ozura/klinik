<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

use App\Models\City;
use App\Models\District;

use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class AddressController extends BaseController{
    
    public function actionChangeProvince(Request $request){
        $input = (object) $request->input();

        try {

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => '',
                'data' => (object) array(
                    'cities' => City::where('province_id', $input->id)->orderBy('name', 'asc')->get()
                )
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }

    public function actionChangeCity(Request $request){
        $input = (object) $request->input();
        
        try {

            return response()->json([
                'status_code' => 200,
                'status_text' => 'Success',
                'message' => '',
                'data' => (object) array(
                    'districts' => District::where('city_id', $input->id)->orderBy('name', 'asc')->get()
                )
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $e->getMessage() : 'Operation error'
            ]);
        }
    }
}