<?php

namespace App\Libraries;

use Carbon\Carbon;

class HelperFunction{

	static function uploadImage($file, $tanggal) {
        $directory = 'uploads/img/'.$tanggal;
        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        $ext = $file->getClientOriginalExtension();
        $filename = rand(100000,1001238912).".".$ext;
        $file->move($directory, $filename);

        $path = $directory.'/'.$filename;
        
        return (object) array(
            'path' => $path,
            'ext' => $ext
        );
        return ;
    }

    static function uploadFile($file, $tanggal) {
        $directory = 'uploads/file/'.$tanggal;
        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        $ext = $file->getClientOriginalExtension();
        $filename = rand(100000,1001238912).".".$ext;
        $file->move($directory, $filename);

        $path = $directory.'/'.$filename;
        
        return (object) array(
            'path' => $path,
            'ext' => $ext
        );
        return ;
    }

    static function uploadBase64File($base64_img, $tanggal) {
        $image_parts = explode(";base64,", $base64_img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $ext = $image_type_aux[1];
        $file = base64_decode($image_parts[1]);

        $directory = 'uploads/img/'.$tanggal;
        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        $path = $directory.'/'.rand(100000,1001238912).".".$ext;
        file_put_contents($path, $file);

        return (object) array(
            'path' => $path,
            'ext' => $ext
        );
    }
    
}