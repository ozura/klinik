<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cart
 */
class Cart extends Model
{
    protected $table = 'carts';

    protected $primaryKey = 'cart_id';

	public $timestamps = true;

    protected $fillable = [
        'seller_id',
        'customer_id',
        'shipping_address_id',
        'shipping_service_id',
        'shipping_price_unit',
        'shipping_etl',
        'payment_method'
    ];

    protected $guarded = [];

    public function customer(){
        return $this->belongsTo('\App\Models\Account', 'customer_id', 'account_id');
    }
}