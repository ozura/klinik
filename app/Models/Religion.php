<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Religion
 */
class Religion extends Model
{
    use SoftDeletes;

    protected $table = 'religions';

    protected $primaryKey = 'religion_id';

	public $timestamps = true;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

}