<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Account
 */
class Account extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'accounts';

    protected $primaryKey = 'account_id';

	public $timestamps = true;

    protected $fillable = [
        'level_id',
        'name',
        'phone',
        'email',
        'username',
        'password',
        'remember_token',
        'image_id',
        'religion_id',
        'gender',
        'birthday',
        'married_status',
        'address_ktp',
        'address_stay',
        'image_aggrement',
        'image_identification',
        'points'
    ];

    protected $guarded = [];

    public function level(){
        return $this->belongsTo('\App\Models\Level', 'level_id')->withTrashed();
    }

    public function photo(){
        return $this->belongsTo('\App\Models\Image', 'image_id')->withTrashed();
    }

    public function aggrement(){
        return $this->belongsTo('\App\Models\Image', 'image_aggrement', 'image_id')->withTrashed();
    }

    public function identification(){
        return $this->belongsTo('\App\Models\Image', 'image_identification', 'image_id')->withTrashed();
    }

    public function ktp(){
        return $this->belongsTo('\App\Models\Address', 'address_ktp', 'address_id')->withTrashed();
    }

    public function stay(){
        return $this->belongsTo('\App\Models\Address', 'address_stay', 'address_id')->withTrashed();
    }

}