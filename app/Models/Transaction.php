<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction
 */
class Transaction extends Model
{
    use SoftDeletes;

    protected $table = 'transactions';

    protected $primaryKey = 'transaction_id';

	public $timestamps = true;

    protected $fillable = [
        'transaction_number',
        'transaction_status',
        'seller_id',
        'customer_id',
        'shipping_address_id',
        'shipping_service_id',
        'payment_method',
        'subtotal',
        'shipping_price',
        'shipping_etl'
    ];

    protected $guarded = [];

    public function customer(){
        return $this->belongsTo('\App\Models\Account', 'customer_id', 'account_id')->withTrashed();
    }

    public function seller(){
        return $this->belongsTo('\App\Models\Account', 'seller_id', 'account_id')->withTrashed();
    }

}