<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Reward
 */
class Reward extends Model
{
    use SoftDeletes;

    protected $table = 'rewards';

    protected $primaryKey = 'reward_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'change_points',
        'available_stock',
        'exchanged_stock',
        'image_id',
        'start_date',
        'end_date',
    ];

    protected $guarded = [];

    public function image(){
        return $this->belongsTo('\App\Models\Image', 'image_id')->withTrashed();
    }
    
}