<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Level
 */
class Level extends Model
{
    use SoftDeletes;

    protected $table = 'levels';

    protected $primaryKey = 'level_id';

	public $timestamps = true;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

    

    


}