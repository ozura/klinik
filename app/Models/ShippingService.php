<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShippingService
 */
class ShippingService extends Model
{
    use SoftDeletes;

    protected $table = 'shipping_services';

    protected $primaryKey = 'shipping_service_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
        'image_url'
    ];

    protected $guarded = [];

    

    


}