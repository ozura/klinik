<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Address
 */
class Address extends Model
{
    use SoftDeletes;

    protected $table = 'addresses';

    protected $primaryKey = 'address_id';

	public $timestamps = true;

    protected $fillable = [
        'type',
        'address',
        'province_id',
        'city_id',
        'district_id',
        'postal_code'
    ];

    protected $guarded = [];

    

    


}