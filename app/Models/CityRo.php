<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CityRo
 */
class CityRo extends Model
{
    use SoftDeletes;

    protected $table = 'cities_ro';

    protected $primaryKey = 'city_id';

	public $timestamps = false;

    protected $fillable = [
        'province_id',
        'province',
        'type',
        'city_name',
        'postal_code'
    ];

    protected $guarded = [];

    

    


}