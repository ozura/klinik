<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductStock
 */
class ProductStock extends Model
{
    protected $table = 'product_stocks';

    protected $primaryKey = 'product_stock_id';

	public $timestamps = true;

    protected $fillable = [
        'account_id',
        'product_id',
        'final_stock',
        'sold_stock'
    ];

    protected $guarded = [];

    public function product(){
        return $this->belongsTo('\App\Models\Product', 'product_id')->withTrashed();
    }

}