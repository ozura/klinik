<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PointsLog
 */
class PointsLog extends Model
{
    use SoftDeletes;

    protected $table = 'points_logs';

    protected $primaryKey = 'points_log_id';

	public $timestamps = true;

    protected $fillable = [
        'transaction_id',
        'account_id',
        'reward_id',
        'status',
        'description'
    ];

    protected $guarded = [];

    public function transaction(){
        return $this->belongsTo('\App\Models\Transaction', 'transaction_id')->withTrashed();
    }
    
    public function account(){
        return $this->belongsTo('\App\Models\Account', 'account_id')->withTrashed();
    }

}