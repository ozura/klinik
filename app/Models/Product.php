<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 */
class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $primaryKey = 'product_id';

	public $timestamps = true;

    protected $fillable = [
        'type',
        'name',
        'harga',
        'persen'
    ];

    protected $guarded = [];

    public function image(){
        return $this->belongsTo('\App\Models\Image', 'image_id')->withTrashed();
    }

    public function purchase_prices(){
        return $this->hasMany('\App\Models\ProductPurchasePrice', 'product_id')->orderBy('level_id', 'asc');
    }
}