<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AccountReward
 */
class AccountReward extends Model
{
    use SoftDeletes;

    protected $table = 'account_rewards';

    protected $primaryKey = 'account_reward_id';

	public $timestamps = true;

    protected $fillable = [
        'account_id',
        'reward_id',
        'status',
    ];

    protected $guarded = [];

    public function account(){
        return $this->belongsTo('\App\Models\Account', 'account_id')->withTrashed();
    }

    public function reward(){
        return $this->belongsTo('\App\Models\Reward', 'reward_id')->withTrashed();
    }

    public function status_to_text(){
        switch($this->status){
            case 'preprocessing': return 'Belum dikonfirmasi'; break;
            case 'onprocessing': return 'Sedang dalam proses'; break;
            case 'done': return 'Selesai'; break;
            default: return 'Oops'; break;
        }
    }
    
}