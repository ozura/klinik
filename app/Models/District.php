<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class District
 */
class District extends Model
{
    use SoftDeletes;

    protected $table = 'districts';

    protected $primaryKey = 'district_id';

	public $timestamps = true;

    protected $fillable = [
        'city_id',
        'code',
        'name'
    ];

    protected $guarded = [];

    

    


}