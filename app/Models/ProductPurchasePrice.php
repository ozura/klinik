<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductPurchasePrice
 */
class ProductPurchasePrice extends Model
{
    use SoftDeletes;

    protected $table = 'product_purchase_prices';

    protected $primaryKey = 'product_purchase_price_id';

	public $timestamps = true;

    protected $fillable = [
        'level_id',
        'product_id',
        'price'
    ];

    protected $guarded = [];

    public function level(){
        return $this->belongsTo('\App\Models\Level', 'level_id')->withTrashed();
    }

}