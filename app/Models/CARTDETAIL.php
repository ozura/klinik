<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartDetail
 */
class CartDetail extends Model
{
    protected $table = 'cart_details';

    protected $primaryKey = 'cart_detail_id';

	public $timestamps = true;

    protected $fillable = [
        'cart_id',
        'product_id',
        'quantity'
    ];

    protected $guarded = [];

    public function product(){
        return $this->belongsTo('\App\Models\Product', 'product_id')->withTrashed();
    }

}