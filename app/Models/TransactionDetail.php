<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TransactionDetail
 */
class TransactionDetail extends Model
{
    use SoftDeletes;

    protected $table = 'transaction_details';

    protected $primaryKey = 'transaction_detail_id';

	public $timestamps = true;

    protected $fillable = [
        'transaction_id',
        'product_id',
        'price',
        'quantity'
    ];

    protected $guarded = [];

    public function product(){
        return $this->belongsTo('\App\Models\Product', 'product_id')->withTrashed();
    }

    public function transaction(){
        return $this->belongsTo('\App\Models\Transaction', 'transaction_id')->withTrashed();
    }

}