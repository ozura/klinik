<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('accounts')->insert(array(
           'type'=>'admin',
           'phone'=>'039381388318',
    		'email'=>'streetbaker2@gmail.com',
    		'username'=>'adebeda',
    		'password'=>Hash::make('adebeda007')    		
        ));
    }
}
